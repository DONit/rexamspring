package ru.donit.rexam

import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import ru.donit.rexam.models.roles.RoleModel
import ru.donit.rexam.models.roles.RoleName
import ru.donit.rexam.models.user.UserModel
import ru.donit.rexam.repositories.RoleModelRepository
import ru.donit.rexam.repositories.UserModelRepository
import ru.donit.rexam.security.SecurityConstants.Companion.ADMIN_LOGIN
import ru.donit.rexam.security.SecurityConstants.Companion.ADMIN_PASSWORD

@Component
class DataLoader(
        private val roleModelRepository: RoleModelRepository,
        private val userModelRepository: UserModelRepository,
        private val bCryptPasswordEncoder: BCryptPasswordEncoder
) : ApplicationListener<ContextRefreshedEvent> {

    var alreadyLoad: Boolean = false

    @Transactional
    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        if(alreadyLoad) return
        if(userModelRepository.findByUsername(ADMIN_LOGIN).isPresent){
            alreadyLoad = true; return
        }else {
            val adminUser = UserModel(username = ADMIN_LOGIN, password = bCryptPasswordEncoder.encode(ADMIN_PASSWORD),
                    email = "donimsi@mail.ru", lastName = "Дони", firstName = "Денис",
                    patronymic = "Романович")
            adminUser.roles = createRolesIfNotFound()
            userModelRepository.save(adminUser)
            alreadyLoad = true
        }
    }

    @Transactional
    fun createRolesIfNotFound(): List<RoleModel> {
        val userRole = roleModelRepository.findByRoleName(RoleName.ROLE_USER)
                .orElse(roleModelRepository.save(RoleModel(roleName = RoleName.ROLE_USER)))
        val adminRole = roleModelRepository.findByRoleName(RoleName.ROLE_ADMIN)
                .orElse(roleModelRepository.save(RoleModel(roleName = RoleName.ROLE_ADMIN)))
        val roles:MutableList<RoleModel> = ArrayList()
        roles.add(userRole)
        roles.add(adminRole)
        return roles
    }

}