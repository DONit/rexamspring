package ru.donit.rexam

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class RexamApplication

fun main(args: Array<String>) {
    runApplication<RexamApplication>(*args)
}