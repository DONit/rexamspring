package ru.donit.rexam.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.answer.AnswerCreateModel
import ru.donit.rexam.services.AnswerModelService
import javax.validation.Valid
import javax.ws.rs.BadRequestException

@RestController
@RequestMapping("/api/answers")
@CrossOrigin(origins = ["http://localhost:8000"])
class AnswerController(
        private val answerModelService: AnswerModelService
) {
    @PostMapping("/create/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun create(@Valid @RequestBody answerCreateModel: AnswerCreateModel, @PathVariable id: Long): ResponseEntity<*> {
        return try {
            answerModelService.create(answerCreateModel, id)
            ResponseEntity("success", HttpStatus.CREATED)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }
    @GetMapping("/detail/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun readById(@PathVariable id: Long): ResponseEntity<*> {
        return try {
            ResponseEntity(answerModelService.readById(id), HttpStatus.OK)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }
}