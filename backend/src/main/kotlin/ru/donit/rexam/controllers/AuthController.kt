package ru.donit.rexam.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.user.UserRegister
import ru.donit.rexam.services.user.UserModelService
import java.lang.Exception
import javax.validation.Valid

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = ["http://localhost:8000"])
class AuthController(
        private val userModelService: UserModelService
) {
    @PostMapping("/register")
    fun register(@Valid @RequestBody userRegister: UserRegister): ResponseEntity<*> {
        return try{
            userModelService.createUser(userRegister)
            ResponseEntity("success", HttpStatus.CREATED)
        }catch (e:Exception){
            ResponseEntity(e.message, HttpStatus.BAD_REQUEST)
        }
    }
}