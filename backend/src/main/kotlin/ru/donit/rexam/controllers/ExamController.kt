package ru.donit.rexam.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.exam.ExamCreateModel
import ru.donit.rexam.models.exam.ExamModel
import ru.donit.rexam.services.ExamModelService
import javax.validation.Valid
import javax.ws.rs.BadRequestException

@RestController
@RequestMapping("/api/exams")
@CrossOrigin(origins = ["http://localhost:8000"])
class ExamController(
        val examModelService: ExamModelService
) {

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun create(@Valid @RequestBody examCreateModel: ExamCreateModel, authentication: Authentication):ResponseEntity<*>{
        return try {
            examModelService.create(examCreateModel, authentication.name)
            ResponseEntity("success", HttpStatus.CREATED)
        }catch (bd:BadRequestException){
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }

    @GetMapping("/all")
    fun readAll(): MutableList<ExamModel> {
        return examModelService.getAllByDate()
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun delete(@PathVariable id: Long):ResponseEntity<*>{
        return try{
            examModelService.deleteById(id)
            ResponseEntity("success", HttpStatus.OK)
        }catch (bd:BadRequestException){
            ResponseEntity.badRequest().body(bd.message)
        }
    }

    @PostMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun update(@Valid @RequestBody examCreateModel: ExamCreateModel, @PathVariable id: Long):ResponseEntity<*>{
        return try{
            examModelService.update(examCreateModel, id)
            ResponseEntity("success", HttpStatus.OK)
        }catch (bd:BadRequestException){
            ResponseEntity.badRequest().body(bd.message)
        }
    }
}