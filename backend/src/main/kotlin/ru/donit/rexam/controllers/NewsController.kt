package ru.donit.rexam.controllers


import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.news.NewsCreateModel
import ru.donit.rexam.models.news.NewsResponseModel
import ru.donit.rexam.models.news.toNewsResponseModel
import ru.donit.rexam.services.NewsModelService
import javax.validation.Valid
import javax.ws.rs.BadRequestException

@RestController
@RequestMapping("/api/news")
@CrossOrigin(origins = ["http://localhost:8000"])
class NewsController(private val newsModelService: NewsModelService) {

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun create(@Valid @RequestBody newsCreateModel: NewsCreateModel, authentication: Authentication): ResponseEntity<String> {
        return try {
            newsModelService.create(newsCreateModel, authentication.name)
            ResponseEntity("success", HttpStatus.CREATED)
        } catch (bd: Exception) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }

    @GetMapping("/all")
    fun readAll(): List<NewsResponseModel> {
        return newsModelService.getAll().map { it.toNewsResponseModel() }
    }

    @PostMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun update(@Valid @RequestBody createModel: NewsCreateModel, @PathVariable id: Long): ResponseEntity<String> {
        return try {
            newsModelService.update(createModel, id)
            ResponseEntity("success", HttpStatus.OK)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun delete(@PathVariable id: Long): ResponseEntity<String> {
        return try {
            newsModelService.delete(id)
            ResponseEntity("success", HttpStatus.OK)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }


}