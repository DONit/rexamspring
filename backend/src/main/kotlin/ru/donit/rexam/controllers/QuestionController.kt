package ru.donit.rexam.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.question.QuestionCreateModel
import ru.donit.rexam.services.QuestionModelService
import javax.validation.Valid
import javax.ws.rs.BadRequestException

@RestController
@RequestMapping("/api/questions")
@CrossOrigin(origins = ["http://localhost:8000"])
class QuestionController(
        private val questionModelService: QuestionModelService
) {

    @PostMapping("/create/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun create(@Valid @RequestBody questionCreateModel: QuestionCreateModel, @PathVariable id: Long):ResponseEntity<*>{
        return try {
            questionModelService.create(questionCreateModel, id)
            ResponseEntity("success", HttpStatus.CREATED)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }
    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun delete(@PathVariable id: Long):ResponseEntity<*>{
        return try {
            questionModelService.delete(id)
            ResponseEntity("success", HttpStatus.CREATED)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }
}