package ru.donit.rexam.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.score.toResponseModel
import ru.donit.rexam.services.ScoreModelService
import javax.ws.rs.BadRequestException

@RestController
@RequestMapping("/api/scores")
@CrossOrigin(origins = ["http://localhost:8000"])
class ScoreController(
        private val scoreModelService: ScoreModelService
) {
    @GetMapping("/create/{examId}/{score}")
    fun create(@PathVariable examId: Long, @PathVariable score: Int, authentication: Authentication): ResponseEntity<*> {
        return try {
            scoreModelService.create(examId, authentication.name, score)
            ResponseEntity("success", HttpStatus.CREATED)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }

    @GetMapping("/all")
    fun readAll(authentication: Authentication): ResponseEntity<*> {
        return try {
            ResponseEntity(scoreModelService.readAllByDate(authentication.name).map { it.toResponseModel() }, HttpStatus.OK)
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }

}