package ru.donit.rexam.controllers

import net.minidev.json.JSONObject
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.validation.Errors
import org.springframework.web.bind.annotation.*
import ru.donit.rexam.models.ValidationError
import ru.donit.rexam.models.response.ValidErrorsResponse
import ru.donit.rexam.models.user.LoginData
import ru.donit.rexam.models.user.PrivateData
import ru.donit.rexam.services.user.UserModelService
import javax.validation.Valid
import javax.validation.ValidationException
import javax.ws.rs.BadRequestException

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = ["http://localhost:8000"])
class UserController(private val userModelService: UserModelService) {

    @GetMapping("/profile")
    fun getCurrentProfile(authentication: Authentication): ResponseEntity<Any> {
        return try {
            ResponseEntity(userModelService.getUserProfile(authentication.name), HttpStatus.OK)
        }catch (bd:BadRequestException){
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }

    @PostMapping("/profile/updateLoginData")
    fun updateLoginData(@Valid @RequestBody loginData: LoginData,authentication: Authentication): ResponseEntity<*> {
        return try {
            val list: List<ValidationError>? = userModelService.updateLoginData(loginData, authentication.name)
            if (list == null) {
                ResponseEntity("success", HttpStatus.OK)
            } else {
                ResponseEntity(list, HttpStatus.BAD_REQUEST)
            }
        } catch (bd: BadRequestException) {
            ResponseEntity(bd.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PostMapping("/profile/updatePrivateData")
    fun updatePrivateData(@Valid @RequestBody privateData: PrivateData, authentication: Authentication): ResponseEntity<String> {
        return try {
            userModelService.updatePrivateData(privateData, authentication.name)
            ResponseEntity("success", HttpStatus.OK)
        }catch (bd:BadRequestException){
            ResponseEntity(bd.message, HttpStatus.BAD_REQUEST)
        }
    }
}