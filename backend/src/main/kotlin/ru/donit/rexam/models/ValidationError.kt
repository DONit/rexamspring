package ru.donit.rexam.models

class ValidationError(val fieldName: String, val message: String)