package ru.donit.rexam.models.answer

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

class AnswerCreateModel (
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Size(max=50, message = "Не больше 50 символов")]
        var answerText: String,
        @field:[Min(0)Max(1)]
        var isTruth:Int
)