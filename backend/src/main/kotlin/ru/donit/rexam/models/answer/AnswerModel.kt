package ru.donit.rexam.models.answer


import com.fasterxml.jackson.annotation.JsonBackReference
import ru.donit.rexam.models.question.QuestionModel
import javax.persistence.*

@Entity
@Table(name = "answers")
class AnswerModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id:Long = 0,

        @Column(name = "truth")
        var truth:Int,

        @Column(name = "answer_text")
        var answerText:String,

        @ManyToOne(fetch = FetchType.LAZY)
        @JsonBackReference
        @JoinColumn(name = "question_id")
        val question: QuestionModel
)