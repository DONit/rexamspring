package ru.donit.rexam.models.exam

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

class ExamCreateModel(
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Length(max=50, message = "Не больше 50 символов")]
        var examHeader: String,
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Length(max = 150,message = "Не больше 150 символов")]
        var examDesc: String,
        @field:[Min(5, message = "Минимум 5 минут!")]
        var duration: Int
)