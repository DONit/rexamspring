package ru.donit.rexam.models.exam

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.annotations.CreationTimestamp
import ru.donit.rexam.models.news.NewsModel
import ru.donit.rexam.models.question.QuestionModel
import ru.donit.rexam.models.score.ScoreModel
import ru.donit.rexam.models.user.UserModel
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "exams")
class ExamModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long = 0,

        @Column(name = "examHeader")
        var examHeader:String,

        @Column(name = "examDesc")
        var examDesc:String,

        @Column(name="duration")
        var duration:Int
        )
{
        @CreationTimestamp
        @Column(name = "createdAt")
        lateinit var createdAt: Date

        @JsonManagedReference
        @OneToMany(mappedBy = "exam", cascade = [CascadeType.ALL], orphanRemoval = true)
        lateinit var questions:List<QuestionModel>

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        lateinit var author: UserModel

        @JsonIgnore
        @OneToMany(mappedBy = "exam", cascade = [CascadeType.ALL], orphanRemoval = true)
        lateinit var score:List<ScoreModel>
}