package ru.donit.rexam.models.news
import org.hibernate.validator.constraints.Length
import javax.validation.constraints.Max
import javax.validation.constraints.NotEmpty

class NewsCreateModel
(
        @field:[NotEmpty(message = "Поле не должно быть пустым") Length(max = 150, message = "Не больше 150 символов!")]
        val newsHeader: String,
        @field:[NotEmpty(message = "Поле не должно быть пустым")]
        val newsContent: String
)