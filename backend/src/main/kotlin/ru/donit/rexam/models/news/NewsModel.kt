package ru.donit.rexam.models.news

import org.hibernate.annotations.CreationTimestamp
import ru.donit.rexam.models.user.UserModel
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "news")
class NewsModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long = 0,

        @Column(name = "news_header")
        var newsHeader: String,

        @Lob
        @Column(name = "news_content", length = 1024)
        var newsContent: String
) {
    @CreationTimestamp
    @Column(name = "createdAt")
    lateinit var createdAt: Date

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    lateinit var author: UserModel
}

fun NewsModel.toNewsResponseModel() = NewsResponseModel(id, newsHeader, newsContent, createdAt, "${author.lastName} ${author.firstName}")