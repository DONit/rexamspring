package ru.donit.rexam.models.news

import java.util.*
import javax.validation.constraints.NotEmpty

class NewsResponseModel(
        @field:[NotEmpty(message="Поле id не может быть пустым!")]
        val id:Long,
        @field:[NotEmpty(message="Поле не может быть пустым!")]
        val newsHeader:String,
        @field:[NotEmpty(message="Поле не может быть пустым!")]
        val newsContent:String,
        @field:[NotEmpty(message="Поле createdAt не может быть пустым!")]
        val createdAt:Date,
        @field:[NotEmpty(message="Поле author не может быть пустым!")]
        val author:String)