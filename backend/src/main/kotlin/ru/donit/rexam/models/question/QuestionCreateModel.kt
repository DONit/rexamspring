package ru.donit.rexam.models.question

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size


class QuestionCreateModel(
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Size(max=50, message = "Не больше 50 символов")]
        var questionText: String,
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Size(max=50, message = "Не больше 50 символов")]
        var answer1Text: String,
        @field:[Min(0) Max(1)]
        var truth1:Int,
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Size(max=50, message = "Не больше 50 символов")]
        var answer2Text: String,
        @field:[Min(0) Max(1)]
        var truth2:Int,
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Size(max=50, message = "Не больше 50 символов")]
        var answer3Text: String,
        @field:[Min(0) Max(1)]
        var truth3:Int,
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Size(max=50, message = "Не больше 50 символов")]
        var answer4Text: String,
        @field:[Min(0) Max(1)]
        var truth4:Int

)