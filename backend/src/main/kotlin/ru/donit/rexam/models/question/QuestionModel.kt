package ru.donit.rexam.models.question

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import ru.donit.rexam.models.answer.AnswerModel
import ru.donit.rexam.models.exam.ExamModel
import javax.persistence.*


@Entity
@Table(name = "questions")
class QuestionModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        @Column(name = "question_text")
        var questionText: String,

        @ManyToOne(fetch = FetchType.LAZY)
        @JsonBackReference
        @JoinColumn(name = "exam_id")
        var exam: ExamModel
) {
        @JsonManagedReference
        @OneToMany(mappedBy = "question", cascade = [CascadeType.ALL], orphanRemoval = true)
        lateinit var answers: List<AnswerModel>
}