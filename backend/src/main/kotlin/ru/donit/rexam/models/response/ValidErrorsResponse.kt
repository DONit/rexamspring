package ru.donit.rexam.models.response

import ru.donit.rexam.models.ValidationError

class ValidErrorsResponse(val errors:List<ValidationError?>)