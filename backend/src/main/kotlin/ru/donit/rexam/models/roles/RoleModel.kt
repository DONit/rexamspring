package ru.donit.rexam.models.roles

import org.hibernate.annotations.NaturalId
import javax.persistence.*

@Entity
@Table(name = "roles")
class RoleModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id:Long = 0,

        @Enumerated(EnumType.STRING)
        @NaturalId
        @Column(name="role_name")
        val roleName:RoleName
)