package ru.donit.rexam.models.roles

enum class RoleName {
    ROLE_ADMIN,
    ROLE_USER
}