package ru.donit.rexam.models.score

import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.CreationTimestamp
import ru.donit.rexam.models.exam.ExamModel
import ru.donit.rexam.models.user.UserModel
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "user_score")
class ScoreModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        @Column(name = "score")
        val score: Int,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        @JsonBackReference
        val user: UserModel,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "exam_id")
        @JsonBackReference
        val exam: ExamModel
) {
    @CreationTimestamp
    @Column(name = "createdAt")
    lateinit var createdAt: Date
}

fun ScoreModel.toResponseModel() = ScoreModelResponse(score, exam.examHeader, createdAt)