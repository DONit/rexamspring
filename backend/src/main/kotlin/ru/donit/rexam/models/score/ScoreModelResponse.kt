package ru.donit.rexam.models.score

import java.util.*

class ScoreModelResponse(
        val score:Int,
        val examHeader:String,
        val date:Date
)