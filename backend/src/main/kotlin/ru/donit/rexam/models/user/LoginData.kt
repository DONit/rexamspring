package ru.donit.rexam.models.user

import ru.donit.rexam.validators.UniqueUsername
import javax.validation.constraints.*

class LoginData(
        @field:[
        NotBlank(message = "Поле не может быть пустым")
        Size(min = 5, max = 15, message = "Имя пользователя должно содержать от 5 до 15 символов")
        Pattern(regexp = "^[a-zA-Z]+[0-9]+$", message = "Поле должно содержать латинские буквы и цифры")
        ]
        val username:String,
        @field:[NotEmpty(message = "Поле не должно быть пустым!") Email(message = "Введите корректный E-Mail")]
        val email:String,
        @field:[
        NotBlank(message = "Поле не может быть пустым")
        Size(min = 5, max = 15, message = "Пароль должен содержать от 5 до 15 символов")
        Pattern(regexp = "^[a-zA-Z]+[0-9]+$", message = "Поле должно содержать латинские буквы и цифры")
        ]
        val password:String
)