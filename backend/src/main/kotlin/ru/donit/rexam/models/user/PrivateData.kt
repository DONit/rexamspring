package ru.donit.rexam.models.user

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

class PrivateData(
        @field:[
        NotBlank(message = "Поле не может быть пустым")
        Size(min = 3, max = 20, message = "От 3 до 20 символов")
        Pattern(regexp = "^[а-яА-Я]+$", message = "Только русские буквы")
        ]
        val firstName: String,

        @field:[
        NotBlank(message = "Поле не может быть пустым")
        Size(min = 3, max = 20, message = "От 3 до 20 символов")
        Pattern(regexp = "^[а-яА-Я]+$", message = "Только русские буквы")
        ]
        val lastName: String,

        @field:[
        NotBlank(message = "Поле не может быть пустым")
        Size(min = 3, max = 20, message = "От 3 до 20 символов")
        Pattern(regexp = "^[а-яА-Я]+$", message = "Только русские буквы")
        ]
        val patronymic: String
)