package ru.donit.rexam.models.user

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty

data class UserCredentials(
        @JsonProperty("username")
        @field:[NotEmpty]
        val username:String,
        @JsonProperty("password")
        @field:[NotEmpty]
        val password:String
)