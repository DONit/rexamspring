package ru.donit.rexam.models.user

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.security.core.authority.SimpleGrantedAuthority
import ru.donit.rexam.models.exam.ExamModel
import ru.donit.rexam.models.news.NewsModel
import ru.donit.rexam.models.roles.RoleModel
import ru.donit.rexam.models.score.ScoreModel
import javax.persistence.*

@Entity
@Table(name = "users")
class UserModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long = 0,

        @Column(name = "username")
        var username: String,

        @Column(name = "email")
        var email:String,

        @Column(name = "password")
        @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
        var password: String,

        @Column(name = "firstName")
        var firstName: String,

        @Column(name = "lastName")
        var lastName: String,

        @Column(name = "patronymic")
        var patronymic: String
){
        @ManyToMany(fetch = FetchType.EAGER)
        @JoinTable(name = "user_roles", joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")])
        lateinit var roles:List<RoleModel>

        @JsonIgnore
        @OneToMany(mappedBy = "author", cascade = [CascadeType.ALL], orphanRemoval = true)
        lateinit var news:List<NewsModel>

        @JsonIgnore
        @OneToMany(mappedBy = "author", cascade = [CascadeType.ALL], orphanRemoval = true)
        lateinit var exams:List<ExamModel>

        @JsonIgnore
        @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], orphanRemoval = true)
        lateinit var score:List<ScoreModel>
}

fun UserModel.toUserProfile() = UserProfile(id,username, firstName, lastName, patronymic, email, roles.map { SimpleGrantedAuthority(it.roleName.name) })