package ru.donit.rexam.models.user

import org.springframework.security.core.GrantedAuthority

class UserProfile(
        val id:Long,
        val username:String,
        val firstName:String,
        val lastName:String,
        val patronymic:String,
        val email:String,
        val roles:List<GrantedAuthority>
)