package ru.donit.rexam.models.user

import ru.donit.rexam.validators.UniqueUsername
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

class UserRegister(@field:[
                   NotBlank(message = "Поле не может быть пустым")
                   Size(min = 5, max = 15, message = "Имя пользователя должно содержать от 5 до 15 символов")
                   UniqueUsername
                   Pattern(regexp = "^[a-zA-Z]+[0-9]+$", message = "Поле должно начинаться с латинских символов и содержать только их и цифры")
                   ]
                   val username: String,

                   @field:[
                   NotBlank(message = "Поле не может быть пустым")
                   Size(min = 5, max = 15, message = "Пароль должен содержать от 5 до 15 символов")
                   Pattern(regexp = "^[a-zA-Z]+[0-9]+$", message = "Поле должно начинаться с латинских символов и содержать только их и цифры")
                   ]
                   val password: String,

                   @field:[
                   NotBlank(message = "Поле не может быть пустым")
                   Size(min = 3, max = 20, message = "От 3 до 20 символов")
                   Pattern(regexp = "^[а-яА-Я]+$", message = "Только русские буквы")
                   ]
                   val firstName: String,

                   @field:[
                   NotBlank(message = "Поле не может быть пустым")
                   Size(min = 3, max = 20, message = "От 3 до 20 символов")
                   Pattern(regexp = "^[а-яА-Я]+$", message = "Только русские буквы")
                   ]
                   val lastName: String,

                   @field:[
                   NotBlank(message = "Поле не может быть пустым")
                   Size(min = 3, max = 20, message = "От 3 до 20 символов")
                   Pattern(regexp = "^[а-яА-Я]+$", message = "Только русские буквы")
                   ]
                   val patronymic: String,

                   @field:[
                   NotBlank(message = "Поле не может быть пустым")
                   Size(max = 20, message = "Не больше 20 символов")
                   Email(message = "Введите корректный E-Mail")
                   ]
                   val email: String)