package ru.donit.rexam.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.donit.rexam.models.answer.AnswerModel

interface AnswerModelRepository: JpaRepository<AnswerModel, Long>