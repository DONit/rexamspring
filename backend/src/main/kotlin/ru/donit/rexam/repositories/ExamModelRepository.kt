package ru.donit.rexam.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.donit.rexam.models.exam.ExamModel

interface ExamModelRepository:JpaRepository<ExamModel, Long>