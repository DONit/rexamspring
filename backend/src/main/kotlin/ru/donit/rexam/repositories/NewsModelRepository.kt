package ru.donit.rexam.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.donit.rexam.models.news.NewsModel

interface NewsModelRepository : JpaRepository<NewsModel, Long>