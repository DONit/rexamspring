package ru.donit.rexam.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.donit.rexam.models.question.QuestionModel

interface QuestionModelRepository: JpaRepository<QuestionModel, Long>