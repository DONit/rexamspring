package ru.donit.rexam.repositories

import org.springframework.data.repository.CrudRepository
import ru.donit.rexam.models.roles.RoleModel
import ru.donit.rexam.models.roles.RoleName
import java.util.*

interface RoleModelRepository:CrudRepository<RoleModel, Long>{
    fun findByRoleName(name:RoleName):Optional<RoleModel>
}