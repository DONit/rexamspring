package ru.donit.rexam.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.donit.rexam.models.score.ScoreModel
import ru.donit.rexam.models.user.UserModel

interface ScoreModelRepository:JpaRepository<ScoreModel,Long>{
    fun findAllByUserOrderByCreatedAtDesc(user:UserModel):List<ScoreModel>
}