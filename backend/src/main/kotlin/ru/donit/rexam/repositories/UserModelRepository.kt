package ru.donit.rexam.repositories

import org.springframework.data.repository.CrudRepository
import ru.donit.rexam.models.user.UserModel
import java.util.*

interface UserModelRepository:CrudRepository<UserModel, Long>{
    fun findByUsername(username:String): Optional<UserModel>
}