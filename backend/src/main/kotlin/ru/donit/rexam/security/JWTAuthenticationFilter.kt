package ru.donit.rexam.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import net.minidev.json.JSONObject
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.donit.rexam.models.user.UserCredentials
import ru.donit.rexam.security.SecurityConstants.Companion.EXPIRATION_TIME
import ru.donit.rexam.security.SecurityConstants.Companion.HEADER_STRING
import ru.donit.rexam.security.SecurityConstants.Companion.SECRET
import ru.donit.rexam.security.SecurityConstants.Companion.TOKEN_PREFIX
import ru.donit.rexam.services.user.UserModelService
import java.io.IOException
import java.lang.RuntimeException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.ws.rs.BadRequestException

class JWTAuthenticationFilter(private val authManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {
        try {
            val userCredentials: UserCredentials = ObjectMapper()
                    .readValue(request?.inputStream, UserCredentials::class.java)
            val token = UsernamePasswordAuthenticationToken(userCredentials.username, userCredentials.password)
            setDetails(request, token)
            return authManager.authenticate(token)
        } catch (e: IOException) {
            throw RuntimeException(e.message)
        }
    }


    override fun successfulAuthentication(request: HttpServletRequest?, response: HttpServletResponse?, chain: FilterChain?, authResult: Authentication?) {
        val userPrincipal = (authResult?.principal as User)
        val token: String = JWT.create()
                .withSubject(userPrincipal.username)
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.toByteArray()))
        val json = JSONObject()
        json.plusAssign(Pair("accessToken", TOKEN_PREFIX + token))
        response?.contentType = "application/json"
        response?.characterEncoding = "UTF-8"
        response?.writer?.write(json.toString())
    }

    override fun unsuccessfulAuthentication(request: HttpServletRequest?, response: HttpServletResponse?, failed: AuthenticationException?) {
        response?.status = 400
    }
}