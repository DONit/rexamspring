package ru.donit.rexam.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.stereotype.Component
import ru.donit.rexam.security.SecurityConstants.Companion.HEADER_STRING
import ru.donit.rexam.security.SecurityConstants.Companion.SECRET
import ru.donit.rexam.security.SecurityConstants.Companion.TOKEN_PREFIX
import ru.donit.rexam.services.user.UserDetailsServiceImpl
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse



class JWTAuthorizationFilter(authenticationManager: AuthenticationManager) : BasicAuthenticationFilter(authenticationManager) {

    @Autowired
    lateinit var userDetailsServiceImpl:UserDetailsServiceImpl

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val header: String? = request.getHeader(HEADER_STRING)
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response)
            return
        }

        val authentication: UsernamePasswordAuthenticationToken? = getAuthentication(request)
        SecurityContextHolder.getContext().authentication = authentication
        chain.doFilter(request, response)
    }

    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token: String? = request.getHeader(HEADER_STRING)
        if (token != null) {
            val username: String? = JWT.require(Algorithm.HMAC512(SECRET.toByteArray()))
                    .build().
                    verify(token.replace(TOKEN_PREFIX, ""))
                    .subject

            if(username != null){
                try {
                    val user: UserDetails = userDetailsServiceImpl.loadUserByUsername(username)
                    return UsernamePasswordAuthenticationToken(user,null, user.authorities)
                }catch (e:UsernameNotFoundException){
                    logger.error(e.message)
                }

            }
            return null
        }
        return null
    }
}