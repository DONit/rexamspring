package ru.donit.rexam.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import ru.donit.rexam.security.SecurityConstants.Companion.SIGN_UP_URL
import ru.donit.rexam.services.user.UserDetailsServiceImpl

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig(
        private val userDetailsServiceImpl: UserDetailsServiceImpl
) : WebSecurityConfigurerAdapter() {
    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder? {
        return BCryptPasswordEncoder()
    }
    @Bean
    fun jwtAuthorizationFilter():JWTAuthorizationFilter{
        return JWTAuthorizationFilter(authenticationManager())
    }

    override fun configure(http: HttpSecurity?) {
        val authFilter = JWTAuthenticationFilter(authenticationManager())
        authFilter.setFilterProcessesUrl("/api/auth/login")
        http?.cors()?.and()?.csrf()?.disable()?.authorizeRequests()
                ?.antMatchers(HttpMethod.POST, SIGN_UP_URL)?.permitAll()
                ?.anyRequest()?.authenticated()
                ?.and()
                ?.addFilter(authFilter)
                ?.addFilter(jwtAuthorizationFilter())
                ?.sessionManagement()?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)


    }

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(userDetailsServiceImpl)?.passwordEncoder(bCryptPasswordEncoder())
    }

}