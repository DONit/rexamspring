package ru.donit.rexam.services

import org.springframework.stereotype.Service
import ru.donit.rexam.models.answer.AnswerCreateModel
import ru.donit.rexam.models.answer.AnswerModel
import ru.donit.rexam.repositories.AnswerModelRepository
import ru.donit.rexam.repositories.QuestionModelRepository
import javax.ws.rs.BadRequestException

@Service
class AnswerModelService(
        private val answerModelRepository: AnswerModelRepository,
        private val questionModelRepository: QuestionModelRepository
) {
    fun create(answerCreateModel: AnswerCreateModel, id: Long): AnswerModel {
        val question = questionModelRepository.findById(id).orElseThrow { BadRequestException("Вопроса с id=$id не существует!") }
        val answer = AnswerModel(answerText = answerCreateModel.answerText, truth = answerCreateModel.isTruth, question = question)
        return answerModelRepository.save(answer)
    }

    fun readById(id: Long): AnswerModel {
        return answerModelRepository.findById(id).orElseThrow { BadRequestException("Ответа с id=$id не существует!") }
    }
}