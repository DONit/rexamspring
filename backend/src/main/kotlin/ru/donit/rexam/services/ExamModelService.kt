package ru.donit.rexam.services

import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import ru.donit.rexam.models.exam.ExamCreateModel
import ru.donit.rexam.models.exam.ExamModel
import ru.donit.rexam.repositories.ExamModelRepository
import ru.donit.rexam.repositories.UserModelRepository
import javax.ws.rs.BadRequestException

@Service
class ExamModelService(
        private val examModelRepository: ExamModelRepository,
        private val userModelRepository: UserModelRepository
){
    fun create(examCreateModel: ExamCreateModel, username:String){
        val user = userModelRepository.findByUsername(username).orElseThrow { BadRequestException("Пользователя $username не существует") }
        val exam = ExamModel(examHeader = examCreateModel.examHeader, examDesc = examCreateModel.examDesc, duration = examCreateModel.duration)
        exam.author = user
        examModelRepository.save(exam)
    }
    fun getAllByDate(): MutableList<ExamModel> {
        return examModelRepository.findAll(Sort.by(Sort.Direction.DESC, "CreatedAt"))
    }
    fun deleteById(id:Long){
        val exam = examModelRepository.findById(id).orElseThrow { BadRequestException("Экзамена с id=${id} не существует!") }
        examModelRepository.delete(exam)
    }
    fun update(examCreateModel: ExamCreateModel, id:Long){
        val exam = examModelRepository.findById(id).orElseThrow { BadRequestException("Экзамена с id=${id} не существует!") }
        exam.examHeader = examCreateModel.examHeader
        exam.examDesc = examCreateModel.examDesc
        exam.duration = examCreateModel.duration
        examModelRepository.save(exam)
    }
}