package ru.donit.rexam.services

import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import ru.donit.rexam.models.news.NewsCreateModel
import ru.donit.rexam.models.news.NewsModel
import ru.donit.rexam.models.user.UserModel
import ru.donit.rexam.repositories.NewsModelRepository
import ru.donit.rexam.repositories.UserModelRepository
import javax.ws.rs.BadRequestException

@Service
class NewsModelService(
        private val newsModelRepository: NewsModelRepository,
        private val userModelRepository: UserModelRepository
) {

    fun getAll(): MutableIterable<NewsModel> {
        return newsModelRepository.findAll(Sort.by(Sort.Direction.DESC, "createdAt"))
    }

    fun update(createModel: NewsCreateModel, id: Long) {
        val news = newsModelRepository.findById(id).orElseThrow { BadRequestException("Новости с id=${id} не существует!") }
        news.newsContent = createModel.newsContent
        news.newsHeader = createModel.newsHeader
        newsModelRepository.save(news)
    }

    fun create(newsCreateModel: NewsCreateModel, username: String): NewsModel {
        val author: UserModel = userModelRepository.findByUsername(username).orElseThrow { BadRequestException("Пользователя $username не существует!") }
        val news = NewsModel(newsHeader = newsCreateModel.newsHeader, newsContent = newsCreateModel.newsContent)
        news.author = author
        return newsModelRepository.save(news)
    }

    fun delete(id: Long) {
        val newsModel: NewsModel = newsModelRepository.findById(id).orElseThrow { BadRequestException("Новости с id=$id не существует!") }
        newsModelRepository.delete(newsModel)
    }
}