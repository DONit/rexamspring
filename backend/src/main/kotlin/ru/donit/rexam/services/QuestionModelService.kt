package ru.donit.rexam.services

import org.springframework.stereotype.Service
import ru.donit.rexam.models.answer.AnswerCreateModel
import ru.donit.rexam.models.answer.AnswerModel
import ru.donit.rexam.models.question.QuestionCreateModel
import ru.donit.rexam.models.question.QuestionModel
import ru.donit.rexam.repositories.AnswerModelRepository
import ru.donit.rexam.repositories.ExamModelRepository
import ru.donit.rexam.repositories.QuestionModelRepository
import javax.ws.rs.BadRequestException

@Service
class QuestionModelService(
        private val questionModelRepository: QuestionModelRepository,
        private val examModelRepository: ExamModelRepository,
        private val answerModelService: AnswerModelService
) {
    fun create(questionCreateModel: QuestionCreateModel, id: Long) {
        val exam = examModelRepository.findById(id).orElseThrow{ BadRequestException("Экзамена с id=$id не существует!") }
        val questionModel = questionModelRepository.save(QuestionModel(questionText = questionCreateModel.questionText, exam = exam))
        answerModelService.create(AnswerCreateModel(answerText = questionCreateModel.answer1Text, isTruth = questionCreateModel.truth1),questionModel.id)
        answerModelService.create(AnswerCreateModel(answerText = questionCreateModel.answer2Text, isTruth = questionCreateModel.truth2),questionModel.id)
        answerModelService.create(AnswerCreateModel(answerText = questionCreateModel.answer3Text, isTruth = questionCreateModel.truth3),questionModel.id)
        answerModelService.create(AnswerCreateModel(answerText = questionCreateModel.answer4Text, isTruth = questionCreateModel.truth4),questionModel.id)
    }
    fun delete(id: Long){
        val question = questionModelRepository.findById(id).orElseThrow { BadRequestException("Вопроса с id=$id не существует!") }
        questionModelRepository.delete(question)
    }
}