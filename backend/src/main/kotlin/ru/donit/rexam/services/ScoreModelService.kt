package ru.donit.rexam.services

import org.springframework.stereotype.Service
import ru.donit.rexam.models.score.ScoreModel
import ru.donit.rexam.repositories.ExamModelRepository
import ru.donit.rexam.repositories.ScoreModelRepository
import ru.donit.rexam.repositories.UserModelRepository
import javax.ws.rs.BadRequestException

@Service
class ScoreModelService(
        private val scoreModelRepository: ScoreModelRepository,
        private val userModelRepository: UserModelRepository,
        private val examModelRepository: ExamModelRepository
) {
    fun create(examId:Long, username:String, score:Int){
        val exam = examModelRepository.findById(examId).orElseThrow { BadRequestException("Экзамена с id=$examId не существует!") }
        val user = userModelRepository.findByUsername(username).orElseThrow { BadRequestException("Пользователя с username=$username не существует!") }
        scoreModelRepository.save(ScoreModel(score = score, user = user, exam = exam))
    }

    fun readAllByDate(username: String): List<ScoreModel> {
        val user = userModelRepository.findByUsername(username).orElseThrow { BadRequestException("Пользователя с username=$username не существует!") }
        return scoreModelRepository.findAllByUserOrderByCreatedAtDesc(user)
    }
}