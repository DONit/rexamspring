package ru.donit.rexam.services.user

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import ru.donit.rexam.models.user.UserModel
import ru.donit.rexam.repositories.UserModelRepository

@Service
class UserDetailsServiceImpl(private val userModelRepository: UserModelRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String?): UserDetails {
        val userModel: UserModel? = username?.let { userModelRepository.findByUsername(it).orElseThrow { UsernameNotFoundException("Пользователь с именем пользователя $username не существует!") } }
        return User(userModel?.username, userModel?.password, userModel?.roles?.map { SimpleGrantedAuthority(it.roleName.name) })
    }

}