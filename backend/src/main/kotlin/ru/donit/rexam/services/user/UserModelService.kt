package ru.donit.rexam.services.user

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import ru.donit.rexam.models.ValidationError
import ru.donit.rexam.models.roles.RoleModel
import ru.donit.rexam.models.roles.RoleName
import ru.donit.rexam.models.user.*
import ru.donit.rexam.repositories.RoleModelRepository
import ru.donit.rexam.repositories.UserModelRepository
import javax.ws.rs.BadRequestException

@Service
class UserModelService(
        private val userModelRepository: UserModelRepository,
        private val bCryptPasswordEncoder: BCryptPasswordEncoder,
        private val roleModelRepository: RoleModelRepository
) {
    fun getUserProfile(username: String): UserProfile{
        val user: UserModel = userModelRepository.findByUsername(username).orElseThrow{ BadRequestException("Пользователя $username не существует") }
        return user.toUserProfile()
    }

    fun updateLoginData(loginData: LoginData, username: String): List<ValidationError>? {
        val user = userModelRepository.findByUsername(username).orElseThrow{BadRequestException("Пользователя $username не существует")}
        user.email = loginData.email
        user.password = bCryptPasswordEncoder.encode(loginData.password)
        if (username != loginData.username) {
            if (usernameAvailable(loginData.username)) {
                user.username = loginData.username
            } else {
                val list: MutableList<ValidationError> = ArrayList()
                list.add(ValidationError("username", message = "Пользователь с таким именем пользователя уже существует!"))
                return list
            }
        }
        userModelRepository.save(user)
        return null
    }

    fun updatePrivateData(privateData: PrivateData, username: String) {
        val user = userModelRepository.findByUsername(username).orElseThrow{BadRequestException("Пользователя $username не существует")}
        user.firstName = privateData.firstName
        user.lastName = privateData.lastName
        user.patronymic = privateData.patronymic
        userModelRepository.save(user)
    }

    fun usernameAvailable(username: String): Boolean {
        return !userModelRepository.findByUsername(username).isPresent
    }

    fun createUser(userRegister: UserRegister) {
        val user = UserModel(username = userRegister.username, email = userRegister.email,
                password = bCryptPasswordEncoder.encode(userRegister.password), firstName = userRegister.firstName,
                lastName = userRegister.lastName, patronymic = userRegister.patronymic)
        val roles: MutableList<RoleModel> = ArrayList()
        val userRole = roleModelRepository.findByRoleName(RoleName.ROLE_USER).orElseThrow{Exception("Ошибка установки прав!")}
        roles.add(userRole)
        user.roles = roles
        userModelRepository.save(user)
    }
}