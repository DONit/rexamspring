package ru.donit.rexam.validators

import ru.donit.rexam.validators.UniqueUsernameValidator
import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass
import kotlin.annotation.Target;
import kotlin.annotation.AnnotationTarget;

@Target(AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.FIELD)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [UniqueUsernameValidator::class])
annotation class UniqueUsername (
    val message: String = "Данное имя пользователя уже существует",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)
