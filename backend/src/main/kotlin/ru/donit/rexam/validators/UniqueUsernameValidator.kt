package ru.donit.rexam.validators

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.donit.rexam.services.user.UserModelService
import javax.validation.ConstraintValidator;
import javax.transaction.Transactional;

import javax.validation.ConstraintValidatorContext

@Component
@Transactional
class UniqueUsernameValidator @Autowired constructor(val userModelService: UserModelService):
        ConstraintValidator<UniqueUsername, String> {

    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        return value?.let { userModelService.usernameAvailable(it) }!!
    }

}
