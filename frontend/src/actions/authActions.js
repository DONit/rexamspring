import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../axios/axios";
import {apiUrls} from "../urls/apiUrls";
import {setRedirect} from "../reducers/redirectReducer";
import {clientUrls} from "../urls/clientUrls";
import {parseErrors} from "../utils/parseHelper";

export const registration = createAsyncThunk("auth/registration", (async (data, thunkAPI) => {
    try {
        const response = await axios.post(apiUrls.authUrls.registration, data)
        thunkAPI.dispatch(setRedirect(clientUrls.auth.login))
        return response.data
    } catch (e) {
        if(e.response === undefined){
            throw {
                name:"Network Error",
                message:"Сервер недоступен!"
            }
        }else{
            const {data} = e.response
            if(data.errors !== undefined){
                parseErrors("Bad Request", data.errors)
            }
        }
        throw "Произошла непредвиденная ошибка!"
    }
}))

export const login = createAsyncThunk("auth/login", (async (data, thunkAPI) => {
    try {
        const response = await axios.post(apiUrls.authUrls.login, data)
        const token = response.data.accessToken
        localStorage.setItem("authorization", token)
        return token
    } catch (e) {
        if(e.response === undefined){
            throw {
                name:"Network Error",
                message:"Сервер недоступен!"
            }
        }
        if (e.response?.status === 400) {
            throw {
                name: "Bad credentials",
                message: "Неверный логин или пароль!",
                data: e.response?.data
            }
        } else {
            throw {
                name: "Internal error",
                message: "Произошла непредвиденная ошибка!",
                data: e.response?.data
            }
        }
    }
}))
