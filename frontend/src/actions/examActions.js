import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../axios/axios";
import {apiUrls} from "../urls/apiUrls";
import {logout} from "../reducers/authReducer";
import {parseErrors} from "../utils/parseHelper";
import {setRedirect} from "../reducers/redirectReducer";
import {clientUrls} from "../urls/clientUrls";
import {getScores} from "./userActions";

export const getExams = createAsyncThunk("exam/getExams", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.get(apiUrls.examUrls.getAll, {headers: {authorization: token}})
        return response.data
    } catch (e) {
        if (e.response === undefined || e?.status === 403) {
            thunkAPI.dispatch(logout())
        } else {
            throw "Ошибка загрузки данных"
        }
    }
}))

export const createExam = createAsyncThunk("exam/createExam", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.post(apiUrls.examUrls.create, data, {headers: {authorization: token}})
        thunkAPI.dispatch(setRedirect(clientUrls.exams.all))
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            const {data} = e.response
            if (data.errors !== undefined) {
                parseErrors("Bad Request", data.errors)
            }
        }
        throw "Произошла непредвиденная ошибка!"
    }
}))

export const deleteExam = createAsyncThunk("exam/deleteExam", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.get(`${apiUrls.examUrls.delete}/${data}`, {headers: {authorization: token}})
        thunkAPI.dispatch(setRedirect(clientUrls.exams.all))
        return response.data
    } catch (e) {
        if (e.response === undefined) thunkAPI.dispatch(logout())
        throw "Произошла непредвиденная ошибка!"
    }
}))

export const updateExam = createAsyncThunk("exam/updateExam", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const exam = {
            examHeader: data.examHeader,
            examDesc: data.examDesc,
            duration: data.duration
        }
        const response = await axios.post(`${apiUrls.examUrls.edit}/${data.id}`, exam, {headers: {authorization: token}})
        thunkAPI.dispatch(getExams())
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            const {data} = e.response
            if (data.errors !== undefined) {
                parseErrors("Bad Request", data.errors)
            }
        }
        throw "Произошла непредвиденная ошибка!"
    }
}))

export const saveScore = createAsyncThunk("exam/saveScore", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.get(`${apiUrls.examUrls.saveScore}/${data.examId}/${data.score}`, {headers: {authorization: token}})
        thunkAPI.dispatch(getScores())
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            throw "Ошибка сохранения стастистики!"
        }
    }
}))