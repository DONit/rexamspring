import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../axios/axios";
import {apiUrls} from "../urls/apiUrls";
import {setRedirect} from "../reducers/redirectReducer";
import {clientUrls} from "../urls/clientUrls";
import {parseErrors} from "../utils/parseHelper";
import {logout} from "../reducers/authReducer";

export const getNews = createAsyncThunk("news/getNews", (async (data, thunkAPI) => {
    const {token} = thunkAPI.getState().authReducer
    const response = await axios.get(
        apiUrls.newsUrls.getAll, {headers: {authorization: token}}
    )
    return response.data
}),{
    condition(arg, api){
        const {newsReducer} = api.getState()
        if(newsReducer.isLoading) return false
    }
})
export const createNews = createAsyncThunk("news/createNews", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.post(apiUrls.newsUrls.create, data, {headers: {authorization: token}})
        thunkAPI.dispatch(setRedirect(clientUrls.pages.home))
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            const {data} = e.response
            if (data.errors !== undefined) {
                parseErrors("Bad Request", data.errors)
            }
        }
        throw "Произошла непредвиденная ошибка!"
    }
}))
export const removeNews = createAsyncThunk("news/removeNews", (async (data, thunkAPI) => {
    const {token} = thunkAPI.getState().authReducer
    const response = await axios.get(`${apiUrls.newsUrls.delete}/${data}`, {headers: {authorization: token}})
    return response.data
}))

export const editNews = createAsyncThunk("news/editNews", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const {id} = data
        delete data.id
        const response = await axios.post(`${apiUrls.newsUrls.edit}/${id}`, data, {headers: {authorization: token}})
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            const {data} = e.response
            if (data.errors !== undefined) {
                parseErrors("Bad Request", data.errors)
            }
        }
        throw "Произошла непредвиденная ошибка!"
    }
}))