import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../axios/axios";
import {apiUrls} from "../urls/apiUrls";
import {getExams} from "./examActions";
import {logout} from "../reducers/authReducer";
import {parseErrors} from "../utils/parseHelper";
import {setRedirect} from "../reducers/redirectReducer";
import {clientUrls} from "../urls/clientUrls";

export const questionCreate = createAsyncThunk("question/questionCreate", (async (data, thunkAPI) => {
    try{
        const {token} = thunkAPI.getState().authReducer
        const {examId, questionData} = data
        console.log(questionData)
        const response = await axios.post(`${apiUrls.examUrls.createQuestion}/${examId}`, questionData, {headers:{authorization:token}})
        thunkAPI.dispatch(getExams())
        return response.data
    }catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            const {data} = e.response
            if (data.errors !== undefined) {
                parseErrors("Bad Request", data.errors)
            }
        }
        throw "Произошла непредвиденная ошибка!"
    }
}))

export const questionDelete = createAsyncThunk("question/questionDelete", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.get(`${apiUrls.examUrls.deleteQuestion}/${data}`, {headers: {authorization: token}})
        thunkAPI.dispatch(getExams())
        return response.data
    } catch (e) {
        if (e.response === undefined) thunkAPI.dispatch(logout())
        throw "Произошла непредвиденная ошибка!"
    }
}))