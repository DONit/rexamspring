import {createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../axios/axios";
import {apiUrls} from "../urls/apiUrls";
import {logout} from "../reducers/authReducer";
import {parseErrors} from "../utils/parseHelper";

export const getUserData = createAsyncThunk("user/getUserData", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.get(apiUrls.userUrls.profile, {headers: {authorization: token}})
        return response.data
    } catch (e) {
        if (e.response?.status === 403 || e.response === undefined) {
            thunkAPI.dispatch(logout())
        }
        throw "Ошибка загрузки данных"
    }
}), {
    condition(arg, api) {
        const {userReducer} = api.getState()
        if (userReducer.isLoading) return false
    }
})
export const updateLoginData = createAsyncThunk("profile/updateLoginData", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.post(apiUrls.userUrls.updateLoginData, data, {headers: {authorization: token}})
        thunkAPI.dispatch(logout())
        return response?.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            switch (e.response?.status) {
                case 403:
                    thunkAPI.dispatch(logout())
                    break;
                case 400:
                    const {data} = e.response
                    if (data.errors !== undefined) {
                        parseErrors("Bad Request", data.errors)
                    } else {
                        parseErrors("Bad Request", data)
                    }
                    break;
            }
        }
        throw "Ошибка обновления данных!"
    }
}))

export const updatePrivateData = createAsyncThunk("profile/updatePrivateData", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.post(apiUrls.userUrls.updatePrivateData, data, {headers: {authorization: token}})
        thunkAPI.dispatch(getUserData())
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            if (e.response.data.errors !== undefined) {
                parseErrors("Bad Request", e.response.data.errors)
            } else {
                throw "Произошла непредвиденная ошибка!"
            }
        }
    }
}))

export const getScores = createAsyncThunk("user/getScores", (async (data, thunkAPI) => {
    try {
        const {token} = thunkAPI.getState().authReducer
        const response = await axios.get(apiUrls.userUrls.getScores, {headers: {authorization: token}})
        return response.data
    } catch (e) {
        if (e.response === undefined) {
            thunkAPI.dispatch(logout())
        } else {
            throw "Ошибка загрузки стастистики!"
        }
    }
}))