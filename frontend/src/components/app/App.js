import React, {Component} from "react"
import RequireAuth from "../auth/RequireAuth/RequireAuth";
import {connect} from "react-redux"
import {Switch, Route} from "react-router"
import {clientUrls} from "../../urls/clientUrls";
import Login from "../auth/Login/Login";
import Logout from "../auth/Logout/Logout";
import Register from "../auth/Register/Register";
import Menu from "../menu/Menu";
import Profile from "../user/profile/Profile";
import ExamCreate from "../exam/ExamCreate";
import ExamList from "../exam/ExamList";
import CreateNews from "../news/CreateNews";
import NewsList from "../news/NewsList";
import ExamDetail from "../exam/ExamDetail";
import Exam from "../exam/Exam";
import ScoresList from "../user/stats/ScoresList";

class App extends Component {
    render() {
        return (
            <Switch>
                <Route path={clientUrls.auth.login} component={Login}/>
                <Route path={clientUrls.auth.register} component={Register}/>
                <Route path={clientUrls.auth.logout} component={Logout}/>
                <RequireAuth>
                    <div className="container-sm mt-md-5">
                        <Menu/>
                            <div className="content-wrapper mb-2">
                                <Route path={clientUrls.user.profile} component={Profile}/>
                                <Route path={clientUrls.exams.create} component={ExamCreate}/>
                                <Route path={clientUrls.exams.all} component={ExamList}/>
                                <Route path={clientUrls.news.create} component={CreateNews}/>
                                <Route exact path={clientUrls.pages.home} component={NewsList}/>
                                <Route exact path={clientUrls.exams.detail} component={ExamDetail}/>
                                <Route exact path={clientUrls.exams.start} component={Exam}/>
                                <Route exact path={clientUrls.user.scores} component={ScoresList}/>
                            </div>
                        <div>
                            <div className="footer shadow-sm mt-1">
                                <div className="footer-content bg-info">Designed by DONit</div>
                            </div>
                        </div>
                    </div>
                </RequireAuth>
            </Switch>
        )
    }
}

export default connect()(App)