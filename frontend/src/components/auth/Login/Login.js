import React, {Component} from "react"
import {connect} from "react-redux"
import {Redirect} from "react-router"
import {clientUrls} from "../../../urls/clientUrls"
import {Form, FormControl, InputGroup, Row, Button} from "react-bootstrap"
import {BsPersonFill, BsLockFill} from "react-icons/all"
import {Link} from "react-router-dom";
import {login} from "../../../actions/authActions";
import {clearError} from "../../../reducers/authReducer";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginData: {
                username: "",
                password: ""
            }
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentWillUnmount() {
        this.props.clearError()
    }

    handleChange(e) {
        const {name, value} = e.target
        const {loginData} = this.state
        this.setState({
            loginData: {
                ...loginData,
                [name]: value
            }
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        const {loginData} = this.state
        this.props.login(loginData)
    }

    render() {
        const {isLogged, isLoading, error} = this.props
        if (isLogged) return (<Redirect to={clientUrls.pages.home}/>)
        if(isLoading) return (<div className="loader"/>)
        return (
            <div className="wrapper">
                <Row className="justify-content-center align-items-center h-100 m-0">
                    <Form className="card shadow-lg p-0 col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3"
                          onSubmit={this.handleSubmit}>
                        <h5 className="card-header bg-info text-center text-white shadow-sm">Авторизация</h5>
                        <div className="card-body d-flex flex-column">
                            {error.trim() !== "" &&
                            <p className="alert alert-danger">
                                {error}
                            </p>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text id="login">
                                        <BsPersonFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Логин" type="text" name="username"
                                             onChange={this.handleChange} value={this.state.loginData.username}/>
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text id="login">
                                        <BsLockFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Пароль" type="password" name="password"
                                             onChange={this.handleChange}/>
                            </InputGroup>
                            <Link to={clientUrls.auth.register}>Ещё не зарегистрировались?</Link>
                            <Button className={"mt-3"} variant={"success"} type={"submit"}>Войти</Button>
                        </div>
                    </Form>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isLogged: state.authReducer.isLogged,
    isLoading: state.authReducer.loading,
    error: state.authReducer.error
})
export default connect(mapStateToProps, {login, clearError})(Login)
