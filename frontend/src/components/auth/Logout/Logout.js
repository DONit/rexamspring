import {connect} from "react-redux"
import {logout} from "../../../reducers/authReducer";
import React,{Component} from "react";
import {clientUrls} from "../../../urls/clientUrls";
import {withRouter} from "react-router";

class Logout extends Component {
    componentDidMount() {
        this.props.logout()
        this.props.history.push(clientUrls.auth.login)
    }

    render() {
        return null
    }
}

export default withRouter(connect(null, {logout})(Logout))