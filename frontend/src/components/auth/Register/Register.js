import React, {Component} from "react";
import {connect} from "react-redux"
import {Button, Form, FormControl, InputGroup, Row} from "react-bootstrap";
import {BsLockFill, BsPersonFill, BsEnvelopeFill, BsPersonLinesFill} from "react-icons/all";
import {Redirect} from "react-router";
import {registration} from "../../../actions/authActions";
import {clearRedirect} from "../../../reducers/redirectReducer";
import {clearError} from "../../../reducers/authReducer";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            registerData: {
                username: "",
                password: "",
                email: "",
                lastName: "",
                firstName: "",
                patronymic: "",
            }
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target
        const {registerData} = this.state
        this.setState({
            registerData: {
                ...registerData,
                [name]: value
            }
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const {registerData} = this.state
        this.props.registration(registerData)
    }

    componentWillUnmount() {
        this.props.clearRedirect()
        this.props.clearError()
    }

    render() {
        const {isLoading, error, redirectUrl, fieldErrors} = this.props
        if (redirectUrl !== null) return (<Redirect to={redirectUrl}/>)
        if (isLoading) return (<div className="loader"/>)
        return (
            <div className="wrapper">
                <Row className="justify-content-center align-items-center h-100 m-0">
                    <Form className="card shadow-lg p-0 col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
                          onSubmit={this.handleSubmit}>
                        <h5 className="card-header bg-info text-center text-white shadow-sm">Регистрация</h5>
                        <div className="card-body d-flex flex-column">
                            {error.trim() !== "" &&
                            <p className="alert alert-danger">
                                {error}
                            </p>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <BsPersonFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Логин" type="text" name="username"
                                             onChange={this.handleChange} value={this.state.registerData.username}
                                             isInvalid={!!fieldErrors.username}/>
                            </InputGroup>
                            {!!fieldErrors.username && <div className="alert alert-danger">{fieldErrors.username}</div>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <BsLockFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Пароль" type="password" name="password"
                                             onChange={this.handleChange} isInvalid={!!fieldErrors.password}/>
                            </InputGroup>
                            {!!fieldErrors.password && <div className="alert alert-danger">{fieldErrors.password}</div>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <BsEnvelopeFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="E-Mail" type="email" name="email"
                                             onChange={this.handleChange} value={this.state.registerData.email}
                                             isInvalid={!!fieldErrors.email}/>
                            </InputGroup>
                            {!!fieldErrors.email && <div className="alert alert-danger">{fieldErrors.email}</div>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <BsPersonLinesFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Фамилия" type="text" name="lastName"
                                             onChange={this.handleChange} value={this.state.registerData.lastName}
                                             isInvalid={!!fieldErrors.lastName}/>
                            </InputGroup>
                            {!!fieldErrors.lastName && <div className="alert alert-danger">{fieldErrors.lastName}</div>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <BsPersonLinesFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Имя" type="text" name="firstName"
                                             onChange={this.handleChange} value={this.state.registerData.firstName}
                                             isInvalid={!!fieldErrors.firstName}/>
                            </InputGroup>
                            {!!fieldErrors.firstName &&
                            <div className="alert alert-danger">{fieldErrors.firstName}</div>}
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <BsPersonLinesFill/>
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl placeholder="Отчество" type="text" name="patronymic"
                                             onChange={this.handleChange} value={this.state.registerData.patronymic}
                                             isInvalid={!!fieldErrors.patronymic}/>
                            </InputGroup>
                            {!!fieldErrors.patronymic &&
                            <div className="alert alert-danger">{fieldErrors.patronymic}</div>}
                            <Button variant={"success"} type={"submit"}>Зарегистрироваться</Button>
                        </div>
                    </Form>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.authReducer.loading,
    error: state.authReducer.error,
    redirectUrl: state.redirectReducer.url,
    fieldErrors: state.authReducer.fieldErrors
})

export default connect(mapStateToProps, {registration, clearRedirect, clearError})(Register)