import React,{Component} from "react";
import {clientUrls} from "../../../urls/clientUrls";
import {Redirect, withRouter} from "react-router"
import {connect} from "react-redux"

class RequireAuth extends Component{

    render() {
        if(this.props.isLogged) {
            return (this.props.children)
        }else {
            return (<Redirect to={clientUrls.auth.login}/>)
        }
    }
}

const mapStateToProps = state =>({
    isLogged: state.authReducer.isLogged
})

export default withRouter(connect(mapStateToProps)(RequireAuth))