import React, {Component, Fragment} from "react";
import {clientUrls} from "../../urls/clientUrls";
import Question from "./question/Question";
import {connect} from "react-redux";
import {getExams, saveScore} from "../../actions/examActions";
import Timer from "./Timer";
import {Button} from "react-bootstrap";

class Exam extends Component {
    constructor(props) {
        super(props);
        this.scores = {}
        this.state = {
            examEnd: false,
            totalScore: 0
        }
    }

    componentDidMount() {
        const {match: {params: {examId}}, exams} = this.props
        if (examId === undefined) this.props.history.push(clientUrls.exams.all)
        if (exams?.length === 0) {
            this.props.getExams()
        }
    }

    handleScore = () => {
        const scoreArray = Object.entries(this.scores)
        let totalScore = 0
        if (scoreArray.length !== 0) {
            for (const [key, value] of scoreArray) {
                totalScore += value
            }
        }
        this.setState({
            examEnd: true,
            totalScore: totalScore
        })
    }

    handleChangeScore = (index, score) => {
        this.scores = {
            ...this.scores,
            [index]: score
        }
    }

    render() {
        const {isLoading, saveScore} = this.props
        const {match: {params: {examId}}, exams} = this.props
        const {examEnd, totalScore} = this.state
        const exam = exams.filter(item => Number(item.id) === Number(examId))[0]
        if (isLoading) return (<div className="loader"/>)
        if (!exam) return (<div className="card card-body">Экзамен не найден!</div>)
        if (examEnd) {
            saveScore({examId:examId, score:totalScore})
            return (<div className="card card-body text-center">
                <h5>Экзамен завершён!</h5>
                <h6>Вы набрали {totalScore} балл(ов).</h6>
            </div>)
        }
        const questions = exam.questions.map((item, key) => {
            return (<Question key={key} index={key + 1} question={item} callback={this.handleChangeScore}/>)
        })
        return (
            <div className="card card-body">
                <h5 className="d-flex justify-content-center">{exam.examHeader}</h5>
                <div className="d-flex justify-content-center"><Timer duration={exam.duration}
                                                                      callback={this.handleScore}/></div>
                {questions}
                <div className="d-flex justify-content-end"><Button variant={"success"}
                                                                    onClick={this.handleScore}>Завершить</Button></div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    exams: state.examReducer.exams,
    isLoading: state.examReducer.isLoading
})

export default connect(mapStateToProps, {getExams, saveScore})(Exam)