import React, {Component} from "react";
import {Button, Form} from "react-bootstrap";
import {clearErrors} from "../../reducers/examReducer";
import {connect} from "react-redux";
import {createExam} from "../../actions/examActions";
import {Redirect} from "react-router";
import {clearRedirect} from "../../reducers/redirectReducer";

class ExamCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exam: {
                examHeader: "",
                examDesc: "",
                duration: ""
            }
        }
    }

    componentWillUnmount() {
        this.props.clearErrors()
        this.props.clearRedirect()
    }

    handleSubmit = event => {
        event.preventDefault()
        const {exam} = this.state
        this.props.createExam(exam)
    }

    handleChange = event => {
        const {name, value} = event.target
        const {exam} = this.state
        this.setState({
            exam: {
                ...exam,
                [name]: value
            }
        })
    }

    render() {
        const {error, fieldErrors, redirectUrl} = this.props
        if (redirectUrl !== null) {
            return (<Redirect to={redirectUrl}/>)
        }
        return (
            <Form className="card card-body" onSubmit={this.handleSubmit}>
                <h5 className="text-center">Добавление экзамена</h5>
                {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                <Form.Group>
                    <Form.Label>Заголовок экзамена</Form.Label>
                    <Form.Control name="examHeader" onChange={this.handleChange} type="text"
                                  placeholder="Заголовок экзамена" isInvalid={!!fieldErrors.examHeader}/>
                    {!!fieldErrors.examHeader &&
                    <div className="alert alert-danger mt-2">{fieldErrors.examHeader}</div>}
                </Form.Group>
                <Form.Group>
                    <Form.Label>Описание экзамена</Form.Label>
                    <Form.Control name="examDesc" onChange={this.handleChange} type="text" as="textarea" rows="3"
                                  placeholder="Описание экзамена" isInvalid={!!fieldErrors.examDesc}/>
                    {!!fieldErrors.examDesc &&
                    <div className="alert alert-danger mt-2">{fieldErrors.examDesc}</div>}
                </Form.Group>
                <Form.Group>
                    <Form.Label>Длительность</Form.Label>
                    <Form.Control name="duration" onChange={this.handleChange} type="text"
                                  placeholder="Длительность в минутах" isInvalid={!!fieldErrors.duration}/>
                    {!!fieldErrors.duration &&
                    <div className="alert alert-danger mt-2">{fieldErrors.duration}</div>}
                </Form.Group>
                <div className="d-flex justify-content-end">
                    <Button variant="success" type="submit">
                        Добавить
                    </Button>
                </div>
            </Form>
        )
    }
}

const mapStateToProps = state => ({
    fieldErrors: state.examReducer.fieldErrors,
    error: state.examReducer.error,
    redirectUrl:state.redirectReducer.url
})

export default connect(mapStateToProps, {createExam, clearErrors, clearRedirect})(ExamCreate)