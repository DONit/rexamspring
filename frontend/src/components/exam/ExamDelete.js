import React, {Component} from "react"
import {FaTrash} from "react-icons/all";
import {Button, Modal} from "react-bootstrap";
import {connect} from "react-redux";
import {deleteExam} from "../../actions/examActions";
import {Redirect} from "react-router";
import {clearRedirect} from "../../reducers/redirectReducer";

class ExamDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openConfirm: false,
        }
    }
    handleRemoveClick = () => {
        this.setState({
            openConfirm: true
        })

    }
    handleHideConfirm = () => {
        this.setState({
            openConfirm: false
        })
    }
    handleConfirmed = id => e => {
        this.props.deleteExam(id)
        this.handleHideConfirm()
    }
    componentWillUnmount() {
        this.props.clearRedirect()
    }

    render() {
        const {exam, redirectUrl} = this.props
        const {openConfirm} = this.state
        if (redirectUrl !== null) {
            return (<Redirect to={redirectUrl}/>)
        }
        return (
            <div className="d-inline">
                <a className="m-1 text-danger link-button" onClick={this.handleRemoveClick}><FaTrash/></a>
                <Modal show={openConfirm} onHide={this.handleHideConfirm}>
                    <Modal.Header>
                        <Modal.Title>Удаление</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Вы действительно хотите удалить "{exam.examHeader}" ?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={"secondary"} onClick={this.handleHideConfirm}>
                            Нет
                        </Button>
                        <Button variant={"danger"} onClick={this.handleConfirmed(exam.id)}>
                            Да
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    redirectUrl: state.redirectReducer.url
})

export default connect(mapStateToProps, {deleteExam, clearRedirect})(ExamDelete)