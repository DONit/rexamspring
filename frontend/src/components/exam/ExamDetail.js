import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {clientUrls} from "../../urls/clientUrls";
import {getExams} from "../../actions/examActions";
import {parseDate} from "../../utils/parseHelper";
import {Button, Row} from "react-bootstrap";
import {rolesConstant} from "../../utils/rolesConstant";
import ExamDelete from "./ExamDelete";
import ExamEdit from "./ExamEdit";
import QuestionList from "./question/QuestionList";
import {Link} from "react-router-dom";


class ExamDetail extends Component {

    componentDidMount() {
        if (this.props.exams?.length === 0) {
            this.props.getExams()
        }
    }

    render() {
        const {match: {params: {examId}}, isLoading, userRoles} = this.props
        const isAdmin = userRoles.length > 0 ? userRoles.some(value => (value.authority === rolesConstant.admin)) : false
        if (examId === undefined) this.props.history.push(clientUrls.exams.all)
        const exam = this.props.exams.filter(item => Number(item.id) === Number(examId))[0]
        if (isLoading) return (<div className="loader"/>)
        if (exam === undefined) return (<div className="card card-body">Экзамен не найден!</div>)
        return (
            <Fragment>
                <div className="card mb-2">
                    <Row className="card-header shadow-sm m-0">
                        {exam.examHeader}
                        <div className="ml-auto">
                            {isAdmin ?
                                <ExamEdit exam={exam}/>
                                : null}
                            {isAdmin ?
                                <ExamDelete exam={exam}/>
                                : null}
                        </div>
                    </Row>
                    <div className="card-body shadow-sm">
                        <p>Описание: {exam.examDesc}</p>
                        <p>Кол-во вопросов: {exam?.questions.length}</p>
                        <p>Длительность: {exam.duration} минут</p>
                        <footer className="blockquote-footer">
                            {
                                `${parseDate(exam.createdAt)} ${exam.author.lastName} ${exam.author.firstName}`
                            }
                        </footer>
                        <div className="d-flex justify-content-end"><Link to={clientUrls.exams.start.replace(":examId", exam.id)} className="btn btn-success">Начать</Link></div>
                    </div>
                </div>
                <QuestionList questions={exam.questions} isAdmin={isAdmin} examId={examId}/>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    exams: state.examReducer.exams,
    isLoading: state.examReducer.isLoading,
    userRoles: state.userReducer.user?.roles
})

export default withRouter(connect(mapStateToProps, {getExams})(ExamDetail))