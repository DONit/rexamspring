import React, {Component} from "react"
import {FaPencilAlt} from "react-icons/all";
import {Button, Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {connect} from "react-redux";
import {updateExam} from "../../actions/examActions";
import {clearErrors} from "../../reducers/examReducer";

class ExamEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDialog: false,
            exam: {
                id: props.exam.id,
                examHeader: props.exam.examHeader,
                examDesc: props.exam.examDesc,
                duration: props.exam.duration
            }
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target;
        const {exam} = this.state;
        this.setState({
            exam: {
                ...exam,
                [name]: value
            }
        });

    }

    handleEditClick = () => {
        this.setState({
            openDialog: true
        })
    }

    handleHideDialog = () => {
        this.setState({
            openDialog: false
        })
    }

    handleConfirmedDialog = e => {
        const {exam} = this.state
        this.props.updateExam(exam)
    }
    componentWillUnmount() {
        this.props.clearErrors()
    }

    render() {
        const {openDialog, exam} = this.state
        const {isLoading, fieldErrors, error} = this.props
        return (
            <div className="d-inline">
                <a className="m-1 text-primary link-button" onClick={this.handleEditClick}><FaPencilAlt/></a>
                <Modal show={openDialog} onHide={this.handleHideDialog}>
                    <Modal.Header>
                        <Modal.Title>Редактирование экзамена</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {isLoading ? <div className="loader"/> :
                            <Form>
                                {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                                <Form.Group>
                                    <Form.Label>Заголовок экзамена</Form.Label>
                                    <Form.Control name="examHeader" onChange={this.handleChange} type="text"
                                                  placeholder="Заголовок новости" value={exam.examHeader}
                                                  isInvalid={!!fieldErrors.examHeader}/>
                                    {!!fieldErrors.examHeader &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.examHeader}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Описание экзамена</Form.Label>
                                    <Form.Control name="examDesc" onChange={this.handleChange} type="text"
                                                  as="textarea"
                                                  rows="3"
                                                  placeholder="Содержание новости" value={exam.examDesc}
                                                  isInvalid={!!fieldErrors.examDesc}/>
                                    {!!fieldErrors.examDesc &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.examDesc}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Длительность</Form.Label>
                                    <Form.Control name="duration" onChange={this.handleChange} type="text"
                                                  placeholder="Длительность в минутах" value={exam.duration}
                                                  isInvalid={!!fieldErrors.duration}/>
                                    {!!fieldErrors.duration &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.duration}</div>}
                                </Form.Group>
                            </Form>}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={"danger"} onClick={this.handleHideDialog}>
                            Отмена
                        </Button>
                        <Button variant={"primary"} onClick={this.handleConfirmedDialog}>
                            Изменить
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    isLoading: state.examReducer.isLoadingEdit,
    fieldErrors: state.examReducer.fieldErrors,
    error: state.examReducer.error
})

export default connect(mapStateToProps,{updateExam, clearErrors})(ExamEdit)