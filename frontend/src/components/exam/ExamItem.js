import React, {Component} from "react";
import {Row} from "react-bootstrap";
import {Fade} from "react-awesome-reveal";
import {clientUrls} from "../../urls/clientUrls";
import {Link} from "react-router-dom";
import {parseDate} from "../../utils/parseHelper";

class ExamItem extends Component {
    render() {
        const {exam} = this.props
        if (exam === null) return null
        return (

            <div className="col mb-3">
                <div className="card shadow-sm">
                    <Link to={clientUrls.exams.detail.replace(":examId", exam.id)} className="text-decoration-none"><Row className="card-header shadow-sm m-0 bg-exam-header text-white">{exam.examHeader}</Row></Link>
                    <div className="card-body">
                        <p className="card-text">{exam.examDesc}</p>
                        <footer className="blockquote-footer">
                            {
                                `${parseDate(exam.createdAt)} ${exam.author.lastName} ${exam.author.firstName}`
                            }
                        </footer>
                    </div>
                </div>
            </div>
        )
    }
}

export default ExamItem