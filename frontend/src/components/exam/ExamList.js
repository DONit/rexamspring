import React, {Component} from "react";
import {connect} from "react-redux";
import {clearErrors} from "../../reducers/examReducer";
import {getExams} from "../../actions/examActions";
import ExamItem from "./ExamItem";
import {rolesConstant} from "../../utils/rolesConstant";
import {Link} from "react-router-dom";
import {clientUrls} from "../../urls/clientUrls";
import {Fade} from "react-awesome-reveal";

class ExamList extends Component {
    componentDidMount() {
        this.props.getExams()
    }

    render() {
        const {exams, isLoading, userRoles} = this.props
        const isAdmin = userRoles.length > 0 ? userRoles.some(value => (value.authority === rolesConstant.admin)) : false
        if (isLoading) return (<div className="loader"/>)
        const examsMap = exams?.map((item, key) => {
            return (<ExamItem exam={item} key={key}/>)
        })
        return (
            <Fade>
                <div>
                    {isAdmin ?
                        <div className="d-flex justify-content-center mb-2">
                            <Link to={clientUrls.exams.create} className="btn btn-success">Добавить экзамен</Link>
                        </div>
                        : null}
                    {exams?.length === 0 ? <p className="card card-body">Экзамены отсутствуют...</p> :
                        <div className="row row-cols-1 row-cols-md-3">
                            {examsMap}
                        </div>
                    }
                </div>
            </Fade>
        )
    }
}

const mapStateToProps = state => ({
    exams: state.examReducer.exams,
    isLoading: state.examReducer.isLoading,
    userRoles: state.userReducer.user?.roles
})

export default connect(mapStateToProps, {clearErrors, getExams})(ExamList)
