import React, {Component, Fragment} from "react";

class Timer extends Component{
    constructor(props) {
        super(props);
        this.state = {
            timeM:props.duration,
            timeS:0
        }
        this.interval = null
    }

    handleTime = () =>{
        let {timeM, timeS} = this.state
        if(timeS === 0){
            if(timeM === 0) {this.props.callback(); return}
            timeM--
            this.setState({
                timeM:timeM,
                timeS: 59
            })
        }else{
            timeS--
            this.setState({
                timeS: timeS
            })
        }
    }
    componentDidMount() {
        this.interval = setInterval(this.handleTime, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        const {timeM, timeS} = this.state
        return(
            <Fragment>
                <h6>Осталось {timeM}:{timeS < 10 ? `0${timeS}`:timeS}</h6>
            </Fragment>
        )
    }
}

export default Timer