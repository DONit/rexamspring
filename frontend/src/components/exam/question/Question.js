import React, {Component, Fragment} from "react";

class Question extends Component {
    constructor(props) {
        super(props);
        this.score = 0
    }

    handleChange = item => e => {
        const {callback, index} = this.props
        if(item.truth === 1){
            if(e.target.checked) {this.score++} else {this.score--}
        }else{
            if(e.target.checked) {this.score--} else {this.score++}
        }
        callback(index, this.score < 0 ? 0:this.score)
    }

    render() {
        const {question, index} = this.props
        const {answers} = question
        if (!question) return null
        if (!answers || answers?.length === 0) return null
        return (
            <Fragment>
                <p>{index}. {question.questionText}</p>
                <ul className="list-unstyled">
                    {answers.map((item, key) => {
                        return (
                            <li key={key}>
                                <input type="checkbox" className="mr-1" onChange={this.handleChange(item)}/><p
                                className="d-inline">{item.answerText}</p>
                            </li>
                        )
                    })}
                </ul>
            </Fragment>

        )
    }
}

export default Question