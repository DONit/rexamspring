import React, {Component} from "react";
import {FaPlus} from "react-icons/all";
import {Button, Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {connect} from "react-redux";
import {clearErrors} from "../../../reducers/quesitonReducer"
import {questionCreate} from "../../../actions/questionActions";

class QuestionCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDialog: false,
            question: {
                questionText: "",
                answer1Text: "",
                truth1: 0,
                answer2Text: "",
                truth2: 0,
                answer3Text: "",
                truth3: 0,
                answer4Text: "",
                truth4: 0,
            }
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target;
        const {question} = this.state;
        this.setState({
            question: {
                ...question,
                [name]: value
            }
        });
    }

    handleChangeCheckBox = (e) => {
        const {name, checked} = e.target
        const {question} = this.state;
        this.setState({
            question: {
                ...question,
                [name]: +checked
            }
        });
    }

    handleEditClick = () => {
        this.setState({
            openDialog: true
        })
    }

    handleHideDialog = () => {
        this.setState({
            openDialog: false
        })
    }

    handleConfirmedDialog = e => {
        const {question} = this.state
        const {examId, questionCreate} = this.props
        questionCreate({questionData:question, examId: examId})
    }

    componentWillUnmount() {
        if(this.props.error.trim() !== "") this.props.clearErrors()
    }

    render() {
        const {openDialog, question} = this.state
        const {isLoading, fieldErrors, error} = this.props
        return (
            <div className="d-inline">
                <a className="m-1 text-success link-button" onClick={this.handleEditClick}><FaPlus/></a>
                <Modal show={openDialog} onHide={this.handleHideDialog}>
                    <Modal.Header>
                        <Modal.Title>Добавление вопроса</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {isLoading ? <div className="loader"/> :
                            <Form>
                                {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                                <Form.Group>
                                    <Form.Label>Текст вопроса</Form.Label>
                                    <Form.Control name="questionText" onChange={this.handleChange} type="text"
                                                  placeholder="Текст вопроса" value={question.questionText}
                                                  isInvalid={!!fieldErrors.questionText}/>
                                    {!!fieldErrors.questionText &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.questionText}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Ответ 1</Form.Label>
                                    <Form.Control name="answer1Text" onChange={this.handleChange} type="text"
                                                  placeholder="Текст ответа" value={question.answer1Text}
                                                  isInvalid={!!fieldErrors.answer1Text}/>
                                    <Form.Check className="mt-1" onChange={this.handleChangeCheckBox} type="checkbox"
                                                label="Верный?" name="truth1" value={question.truth1}/>
                                    {!!fieldErrors.answer1Text &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.answer1Text}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Ответ 2</Form.Label>
                                    <Form.Control name="answer2Text" onChange={this.handleChange} type="text"
                                                  placeholder="Текст ответ" value={question.answer2Text}
                                                  isInvalid={!!fieldErrors.answer2Text}/>
                                    <Form.Check className="mt-1" onChange={this.handleChangeCheckBox} type="checkbox"
                                                label="Верный?" name="truth2" value={question.truth2}/>
                                    {!!fieldErrors.answer2Text &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.answer2Text}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Ответ 3</Form.Label>
                                    <Form.Control name="answer3Text" onChange={this.handleChange} type="text"
                                                  placeholder="Текст ответ" value={question.answer3Text}
                                                  isInvalid={!!fieldErrors.answer3Text}/>
                                    <Form.Check className="mt-1" onChange={this.handleChangeCheckBox} type="checkbox"
                                                label="Верный?" name="truth3" value={question.truth3}/>
                                    {!!fieldErrors.answer3Text &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.answer3Text}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Ответ 4</Form.Label>
                                    <Form.Control name="answer4Text" onChange={this.handleChange} type="text"
                                                  placeholder="Текст ответа" value={question.answer4Text}
                                                  isInvalid={!!fieldErrors.answer4Text}/>
                                    <Form.Check className="mt-1" onChange={this.handleChangeCheckBox} type="checkbox"
                                                label="Верный?" name="truth4" value={question.truth4}/>
                                    {!!fieldErrors.answer4Text &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.answer4Text}</div>}
                                </Form.Group>
                            </Form>}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={"danger"} onClick={this.handleHideDialog}>
                            Отмена
                        </Button>
                        <Button variant={"primary"} onClick={this.handleConfirmedDialog}>
                            Добавить
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    fieldErrors: state.questionReducer.fieldErrors,
    error: state.questionReducer.error,
    isLoading: state.questionReducer.isLoading
})

export default connect(mapStateToProps, {clearErrors, questionCreate})(QuestionCreate)