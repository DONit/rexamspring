import React, {Component} from "react"
import {FaTrash} from "react-icons/all";
import {Button, Modal} from "react-bootstrap";
import {connect} from "react-redux";
import {questionDelete} from "../../../actions/questionActions";


class QuestionDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openConfirm: false,
        }
    }
    handleRemoveClick = () => {
        this.setState({
            openConfirm: true
        })

    }
    handleHideConfirm = () => {
        this.setState({
            openConfirm: false
        })
    }
    handleConfirmed = id => e => {
        this.props.questionDelete(id)
        this.handleHideConfirm()
    }

    render() {
        const {question} = this.props
        const {openConfirm} = this.state
        return (
            <div className="d-inline">
                <a className="m-1 text-danger link-button" onClick={this.handleRemoveClick}><FaTrash/></a>
                <Modal show={openConfirm} onHide={this.handleHideConfirm}>
                    <Modal.Header>
                        <Modal.Title>Удаление</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Вы действительно хотите удалить "{question.questionText}" ?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={"secondary"} onClick={this.handleHideConfirm}>
                            Нет
                        </Button>
                        <Button variant={"danger"} onClick={this.handleConfirmed(question.id)}>
                            Да
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default connect(null, {questionDelete})(QuestionDelete)