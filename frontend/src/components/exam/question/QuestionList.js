import React, {Component} from "react"
import {Row} from "react-bootstrap";
import QuestionCreate from "./QuestionCreate";
import QuestionDelete from "./QuestionDelete";

class QuestionList extends Component {
    render() {
        const {questions, isAdmin} = this.props
        if (!questions) return null
        return (
            <div className="card">
                <Row className="card-header shadow-sm m-0">Список вопросов
                    <div className="ml-auto">
                        {isAdmin ?
                            <QuestionCreate examId={this.props.examId}/>
                            : null}
                    </div>
                </Row>
                {questions.length === 0 ? <p className="card-body">Вопросы отсуствуют</p> :
                    <div className="card-body">
                        {questions.map((item, key) => {
                            return (<Row className="m-0" key={key}><p>{key+1}. {item.questionText}</p><div className="ml-auto"><QuestionDelete question={item}/></div></Row>)
                        })}
                    </div>
                }
            </div>
        )
    }
}

export default QuestionList