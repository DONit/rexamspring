import React, {Component} from "react";
import {connect} from "react-redux";
import {Navbar, Nav, NavDropdown} from "react-bootstrap"
import {Link} from "react-router-dom"
import {BsPersonFill} from "react-icons/all";
import {clientUrls} from "../../urls/clientUrls";
import {getUserData} from "../../actions/userActions";

class Menu extends Component {
    componentDidMount() {
        this.props.getUserData()
    }

    render() {
        const {isLoading, error, userFullName} = this.props
        return (
            <div>
                <Navbar className="menu mb-2" bg="info" variant="dark" expand={"md"}>
                    <Navbar.Brand as={Link} to={clientUrls.pages.home}>RExam</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to={clientUrls.pages.home} className="active">Главная</Nav.Link>
                            <Nav.Link as={Link} to={clientUrls.exams.all} className="active">Экзамены</Nav.Link>
                        </Nav>
                        <Nav className="justify-content-end">
                            <NavDropdown className="active mr-auto" title={
                                <span>{isLoading ? "Loading..." : userFullName?.trim()?.length === 0 ? null:<div className="d-inline"><BsPersonFill/>{userFullName}</div>}{error?.trim() !== "" ? <div className="d-inline alert alert-danger">{error}</div> : null}</span>}
                                         id="basic-nav-dropdown">
                                <NavDropdown.Item as={Link} to={clientUrls.user.profile}>Профиль</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to={clientUrls.user.scores}>Статистика</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item as={Link} to={clientUrls.auth.logout}>Выйти</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.userReducer.isLoading,
    error: state.userReducer.error,
    userFullName: state.userReducer.user?.lastName + " " + state.userReducer.user?.firstName,
})

export default connect(mapStateToProps, {getUserData})(Menu)