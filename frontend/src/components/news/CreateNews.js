import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import {clearErrors} from "../../reducers/newsReducer";
import {clearRedirect} from "../../reducers/redirectReducer";
import {Redirect} from "react-router";
import {createNews, getNews} from "../../actions/newsActions";

class CreateNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: {
                newsHeader: "",
                newsContent: ""
            }
        }
    }

    handleChange = event => {
        const {name, value} = event.target;
        const {news} = this.state;
        this.setState({
            news: {
                ...news,
                [name]: value
            }
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        const {news} = this.state
        this.props.createNews(news)
    }

    componentWillUnmount() {
        this.props.clearRedirect()
        this.props.clearErrors()
    }

    render() {
        const {isLoading, redirectUrl, error, fieldErrors} = this.props
        if (redirectUrl !== null) {
            this.props.getNews()
            return (<Redirect to={redirectUrl}/>)
        }
        if (isLoading) return (<div className="loader"/>)
        return (
            <div className="card">
                <Form className="card-body" onSubmit={this.handleSubmit}>
                    <h5 className="text-center">Добавление новости</h5>
                    {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                    <Form.Group>
                        <Form.Label>Заголовок новости</Form.Label>
                        <Form.Control name="newsHeader" onChange={this.handleChange} type="text"
                                      placeholder="Заголовок новости" isInvalid={!!fieldErrors.newsHeader}/>
                        {!!fieldErrors.newsHeader && <div className="alert alert-danger mt-2">{fieldErrors.newsHeader}</div>}
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Содержание новости</Form.Label>
                        <Form.Control name="newsContent" onChange={this.handleChange} type="text" as="textarea" rows="3"
                                      placeholder="Содержание новости" isInvalid={!!fieldErrors.newsContent}/>
                        {!!fieldErrors.newsContent && <div className="alert alert-danger mt-2">{fieldErrors.newsContent}</div>}
                    </Form.Group>
                    <div className="d-flex justify-content-end">
                        <Button variant="success" type="submit">
                            Добавить
                        </Button>
                    </div>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.newsReducer.isLoading,
    redirectUrl: state.redirectReducer.url,
    fieldErrors: state.newsReducer.fieldErrors,
    error: state.newsReducer.error
})

export default connect(mapStateToProps, {createNews, clearRedirect, getNews, clearErrors})(CreateNews)