import React, {Component} from "react";
import {Button, Modal} from "react-bootstrap";
import {FaTrash} from "react-icons/all";
import {connect} from "react-redux";
import {getNews, removeNews} from "../../actions/newsActions";

class NewsDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openConfirm: false,
        }
    }
    handleRemoveClick = () => {
        this.setState({
            openConfirm: true
        })

    }
    handleHideConfirm = () => {
        this.setState({
            openConfirm: false
        })
    }
    handleConfirmed = id => e => {
        this.props.removeNews(id)
        this.handleHideConfirm()
    }
    shouldComponentUpdate(nextProps, nextState, nextContext){
        if(nextProps.deleted){
            nextProps.getNews()
        }
        return true
    }

    render() {
        const {news} = this.props
        const {openConfirm} = this.state
        return (
            <div className="d-inline">
                <a className="m-1 text-danger link-button" onClick={this.handleRemoveClick}><FaTrash/></a>
                <Modal show={openConfirm} onHide={this.handleHideConfirm}>
                    <Modal.Header>
                        <Modal.Title>Удаление</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Вы действительно хотите удалить "{news.newsHeader}" ?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={"secondary"} onClick={this.handleHideConfirm}>
                            Нет
                        </Button>
                        <Button variant={"danger"} onClick={this.handleConfirmed(news.id)}>
                            Да
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = state =>({
    deleted:state.newsReducer.deleted
})

export default connect(mapStateToProps, {removeNews, getNews})(NewsDelete)