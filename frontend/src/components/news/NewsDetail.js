import React, {Component} from "react";
import {connect} from "react-redux";
import {rolesConstant} from "../../utils/rolesConstant";
import NewsDelete from "./NewsDelete";
import NewsEdit from "./NewsEdit";
import Row from "react-bootstrap/Row";
import {Fade} from "react-awesome-reveal";

class NewsDetail extends Component {

    render() {
        const {news, userRoles} = this.props
        const isAdmin = userRoles.length > 0 ? userRoles.some(value => (value.authority === rolesConstant.admin)) : false
        if (!news) return null
        return (
            <Fade>
                <div className="card shadow-sm mb-2">
                    <Row className="card-header shadow-sm m-0">
                        {news.newsHeader}
                        <div className="ml-auto">
                            {isAdmin ?
                                <NewsEdit news={news}/>
                                : null}
                            {isAdmin ?
                                <NewsDelete news={news}/>
                                : null}
                        </div>
                    </Row>
                    <div className="card-body">
                        <p>{news.newsContent}</p>
                        <footer className="blockquote-footer">
                            {new Intl.DateTimeFormat('ru', {
                                year: 'numeric',
                                month: '2-digit',
                                day: '2-digit',
                                hour: '2-digit',
                                minute: '2-digit'
                            }).format(Date.parse(news.createdAt)) + " " + news.author}
                        </footer>
                    </div>
                </div>
            </Fade>
        )
    }
}

const mapStateToProps = state => ({
    userRoles: state.userReducer.user?.roles
})

export default connect(mapStateToProps)(NewsDetail)