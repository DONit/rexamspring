import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import {connect} from "react-redux";
import {Button, Modal} from "react-bootstrap";
import {clearErrors, clearSuccess} from "../../reducers/newsReducer";
import {FaPencilAlt} from "react-icons/all";
import {editNews, getNews} from "../../actions/newsActions";

class NewsEdit extends Component {
    constructor(props) {
        super(props);
        const {news} = this.props
        this.state = {
            news: {
                id: news.id,
                newsHeader: news.newsHeader,
                newsContent: news.newsContent,
            },
            openDialog: false,
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target;
        const {news} = this.state;
        this.setState({
            news: {
                ...news,
                [name]: value
            }
        });
    }

    handleEditClick = () => {
        this.setState({
            openDialog: true
        })

    }
    handleHideDialog = () => {
        this.setState({
            openDialog: false
        })
    }
    handleConfirmedDialog = data => e => {
        this.props.editNews(data)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.successChange) {
            nextState.openDialog = false
            nextProps.getNews()
            nextProps.clearSuccess()
            nextProps.clearErrors()
        }
        return true
    }
    componentWillUnmount() {
        if(this.props.error.trim() !== "")this.props.clearErrors()
    }

    render() {
        const {isLoading, error, fieldErrors} = this.props
        const {news, openDialog} = this.state
        if (isLoading) return (<div className="loader"/>)
        return (
            <div className="d-inline">
                <a className="m-1 text-primary link-button" onClick={this.handleEditClick}><FaPencilAlt/></a>
                <Modal show={openDialog} onHide={this.handleHideDialog}>
                    <Modal.Header>
                        <Modal.Title>Редактирование новости</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {isLoading ? <div className="loader"/> :
                            <Form>
                                {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                                <Form.Group>
                                    <Form.Label>Заголовок новости</Form.Label>
                                    <Form.Control name="newsHeader" onChange={this.handleChange} type="text"
                                                  placeholder="Заголовок новости" value={news.newsHeader} isInvalid={!!fieldErrors.newsHeader}/>
                                    {!!fieldErrors.newsHeader &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.newsHeader}</div>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Содержание новости</Form.Label>
                                    <Form.Control name="newsContent" onChange={this.handleChange} type="text"
                                                  as="textarea"
                                                  rows="3"
                                                  placeholder="Содержание новости" value={news.newsContent} isInvalid={!!fieldErrors.newsContent}/>
                                    {!!fieldErrors.newsContent &&
                                    <div className="alert alert-danger mt-2">{fieldErrors.newsContent}</div>}
                                </Form.Group>
                            </Form>}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={"danger"} onClick={this.handleHideDialog}>
                            Отмена
                        </Button>
                        <Button variant={"primary"} onClick={this.handleConfirmedDialog(news)}>
                            Изменить
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.newsReducer.isLoadingChange,
    successChange: state.newsReducer.successChange,
    fieldErrors: state.newsReducer.fieldErrors,
    error: state.newsReducer.error
})

export default connect(mapStateToProps, {editNews, getNews, clearSuccess, clearErrors})(NewsEdit)