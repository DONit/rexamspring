import React, {Component} from "react";
import {connect} from "react-redux"
import NewsDetail from "./NewsDetail";
import {rolesConstant} from "../../utils/rolesConstant";
import {Link} from "react-router-dom";
import {clientUrls} from "../../urls/clientUrls";
import {getNews} from "../../actions/newsActions";
import {clearErrors} from "../../reducers/newsReducer";
import {Fade} from "react-awesome-reveal";

class NewsList extends Component {
    componentDidMount() {
        this.props.getNews()
    }
    componentWillUnmount() {
        if(this.props.error.trim() !== "")this.props.clearErrors()
    }

    render() {
        const {isLoading, newsList, error, userRoles} = this.props
        const isAdmin = userRoles?.length > 0 ? userRoles.some(value => (value.authority === rolesConstant.admin)) : false
        if (isLoading) return (<div className="loader"/>)
        if (error.trim() !== "") return (<div className="alert alert-danger">{error}</div>)
        const newsListMap = newsList.map((item, key) => {
            return (<NewsDetail news={item} key={key}/>)
        })
        return (
            <Fade>
            <div>
                    {isAdmin ?
                    <div className="d-flex justify-content-center mb-2">
                        <Link to={clientUrls.news.create} className="btn btn-success">Добавить новость</Link>
                    </div>
                        : null}
                {newsList.length !== 0 ? newsListMap:<p className="card card-body">Новости отсутствуют...</p> }
            </div>
            </Fade>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.newsReducer.isLoading,
    newsList: state.newsReducer.newsList,
    error: state.newsReducer.listError,
    userRoles: state.userReducer.user?.roles,
})

export default connect(mapStateToProps, {getNews, clearErrors})(NewsList)