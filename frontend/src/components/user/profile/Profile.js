import React, {Component} from "react";
import {connect} from "react-redux";
import {Col} from "react-bootstrap";
import UpdateLoginData from "./UpdateLoginData";
import UpdatePrivateData from "./UpdatePrivateData";
import {logout} from "../../../reducers/authReducer";
import {withRouter} from "react-router";
import {getUserData} from "../../../actions/userActions";
import {clearLoginDataSuccess, clearPrivateDataSuccess} from "../../../reducers/profileReducer";

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginDataEditing: false,
            privateDataEditing: false
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.successPrivateData) {
            this.setState({
                privateDataEditing: false
            })
            this.props.clearPrivateDataSuccess()
        }
    }

    render() {
        const {
            error, isLoading, userData
        } = this.props
        const {loginDataEditing, privateDataEditing} = this.state
        if (error.trim() !== "") return (<div className="alert alert-danger">{error}</div>)
        if (isLoading) return (<div className="loader"/>)
        return (
            <div>
                <Col className="card p-0 mb-2 shadow-sm">
                    <h5 className="card-header shadow-sm my-card-header">
                        <div className="d-flex flex-row">
                            <div className="mt-auto mb-auto">Безопасность и вход:</div>
                            <button className="btn btn-mySecondary ml-auto" onClick={loginDataEditing ? () => {
                                this.setState({loginDataEditing: false})
                            } : () => {
                                this.setState({loginDataEditing: true})
                            }}>{loginDataEditing ? "Отмена" : "Редактировать"}</button>
                        </div>
                    </h5>
                    {loginDataEditing ? <UpdateLoginData userData={userData}/>
                        : <div className="card-body">
                            <p>Логин: {userData.username}</p>
                            <p>E-Mail: {userData.email}</p>
                        </div>}
                </Col>
                <Col className="card p-0 shadow-sm">

                    <h5 className="card-header shadow-sm my-card-header">
                        <div className="d-flex flex-row">
                            <div className="mt-auto mb-auto">Личные данные:</div>
                            <button className="btn btn-mySecondary ml-auto" onClick={privateDataEditing ? () => {
                                this.setState({privateDataEditing: false})
                            } : () => {
                                this.setState({privateDataEditing: true})
                            }}>{privateDataEditing ? "Отмена" : "Редактировать"}</button>
                        </div>
                    </h5>
                    {privateDataEditing ? <UpdatePrivateData userData={userData}/> :
                        <div className="card-body">
                            <p>Фамилия: {userData.lastName}</p>
                            <p>Имя: {userData.firstName}</p>
                            <p>Отчество: {userData.patronymic}</p>
                        </div>}
                </Col>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userData: state.userReducer.user,
    isLoading: state.userReducer.isLoading,
    error: state.userReducer.error,
    successLoginData: state.profileReducer.successLoginData,
    successPrivateData: state.profileReducer.successPrivateData
})

export default withRouter(connect(mapStateToProps, {getUserData,logout, clearPrivateDataSuccess})(Profile))