import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Form, FormControl, InputGroup} from "react-bootstrap";
import {BsEnvelopeFill, BsLockFill, BsPersonFill} from "react-icons/all";
import {updateLoginData} from "../../../actions/userActions";
import {clearErrorsLoginData} from "../../../reducers/profileReducer";

class UpdateLoginData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginData: {
                username: props.userData.username,
                email: props.userData.email,
                password: ""
            }
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target
        const {loginData} = this.state
        this.setState({
            loginData: {
                ...loginData,
                [name]: value
            }
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const {loginData} = this.state
        this.props.updateLoginData(loginData)
    }


    componentWillUnmount() {
        this.props.clearErrorsLoginData()
    }

    render() {
        const {isLoading, fieldErrors, error} = this.props
        const {loginData} = this.state
        if(isLoading) return (<div className="loader"/>)
        return (
            <Form className="card-body" onSubmit={this.handleSubmit}>
                {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <BsPersonFill/>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl placeholder="Логин" type="text" name="username"
                                 onChange={this.handleChange} value={loginData.username}
                                 isInvalid={!!fieldErrors.username}/>
                </InputGroup>
                {!!fieldErrors.username &&
                <div className="alert alert-danger">{fieldErrors.username}</div>}
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <BsEnvelopeFill/>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl placeholder="E-Mail" type="email" name="email"
                                 onChange={this.handleChange} value={loginData.email}
                                 isInvalid={!!fieldErrors.email}/>
                </InputGroup>
                {!!fieldErrors.email &&
                <div className="alert alert-danger">{fieldErrors.email}</div>}
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <BsLockFill/>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl placeholder="Пароль" type="password" name="password"
                                 onChange={this.handleChange}
                                 isInvalid={!!fieldErrors.password}/>
                </InputGroup>
                {!!fieldErrors.password &&
                <div className="alert alert-danger">{fieldErrors.password}</div>}
                <Button variant={"success"} type={"submit"}>Сохранить</Button>
            </Form>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.profileReducer.isLoadingLoginData,
    fieldErrors: state.profileReducer.fieldErrorsLoginData,
    error:state.profileReducer.errorLoginData,
})

export default connect(mapStateToProps,{updateLoginData, clearErrorsLoginData})(UpdateLoginData)