import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Form, FormControl, InputGroup} from "react-bootstrap";
import {BsEnvelopeFill, BsLockFill, BsPersonFill} from "react-icons/all";
import {updatePrivateData} from "../../../actions/userActions";
import {clearErrorsPrivateData} from "../../../reducers/profileReducer";

class UpdatePrivateData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            privateData: {
                firstName: props.userData.firstName,
                lastName: props.userData.lastName,
                patronymic: props.userData.patronymic
            }
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target
        const {privateData} = this.state
        this.setState({
            privateData: {
                ...privateData,
                [name]: value
            }
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const {privateData} = this.state
        this.props.updatePrivateData(privateData)
    }
    componentWillUnmount() {
        this.props.clearErrorsPrivateData()
    }
    render() {
        const {isLoading, fieldErrors, error} = this.props
        const {privateData} = this.state
        if(isLoading) return (<div className="loader"/>)
        return (
            <Form className="card-body" onSubmit={this.handleSubmit}>
                {error?.trim() !== "" ? <div className="alert alert-danger">{error}</div> : null}
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <BsPersonFill/>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl placeholder="Фамилия" type="text" name="lastName"
                                 onChange={this.handleChange} value={privateData.lastName}
                                 isInvalid={!!fieldErrors.lastName}/>
                </InputGroup>
                {!!fieldErrors.lastName &&
                <div className="alert alert-danger">{fieldErrors.lastName}</div>}
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <BsEnvelopeFill/>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl placeholder="Имя" type="text" name="firstName"
                                 onChange={this.handleChange} value={privateData.firstName}
                                 isInvalid={!!fieldErrors.firstName}/>
                </InputGroup>
                {!!fieldErrors.firstName &&
                <div className="alert alert-danger">{fieldErrors.firstName}</div>}
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <BsLockFill/>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl placeholder="Отчество" type="text" name="patronymic" value={privateData.patronymic}
                                 onChange={this.handleChange}
                                 isInvalid={!!fieldErrors.patronymic}/>
                </InputGroup>
                {!!fieldErrors.patronymic &&
                <div className="alert alert-danger">{fieldErrors.patronymic}</div>}
                <Button variant={"success"} type={"submit"}>Сохранить</Button>
            </Form>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.profileReducer.isLoadingPrivateData,
    fieldErrors: state.profileReducer.fieldErrorsPrivateData,
    error:state.profileReducer.errorPrivateData
})

export default connect(mapStateToProps,{updatePrivateData, clearErrorsPrivateData})(UpdatePrivateData)