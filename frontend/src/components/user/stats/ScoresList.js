import React, {Component, Fragment} from "react"
import {connect} from "react-redux";
import {getScores} from "../../../actions/userActions";
import {parseDate} from "../../../utils/parseHelper";

class ScoresList extends Component {
    componentDidMount() {
        this.props.getScores()
    }

    render() {
        const {error, scores, isLoading} = this.props
        if (error.trim() !== "") return (<div className="alert alert-danger">{error}</div>)
        if (isLoading) return (<div className="loader"/>)
        if (scores.length === 0) return (<div className="card card-body">Статистика отсутствует</div>)
        return (
            <div className="table-responsive">
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col" className="text-center">№</th>
                        <th scope="col">Название экзамена</th>
                        <th scope="col" className="text-center">Кол-во баллов</th>
                        <th scope="col" className="text-center">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    {scores.map((item, key) => {
                        return (
                            <tr key={key}>
                                <td className="text-center">{key + 1}</td>
                                <td>{item.examHeader}</td>
                                <td className="text-center">{item.score}</td>
                                <td className="text-center">{parseDate(item.date)}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.userReducer.isLoadingScore,
    scores: state.userReducer.scores,
    error: state.userReducer.errorScore
})
export default connect(mapStateToProps, {getScores})(ScoresList)