import React from "react"
import ReactDOM from "react-dom"
import {BrowserRouter as Router} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store/store";
import App from "./components/app/App";
import "./index.css"
import "bootstrap/dist/css/bootstrap.min.css"

let root = document.getElementById("root")
ReactDOM.render(
        <Provider store={store}>
            <Router>
                <App/>
            </Router>
        </Provider>,
    root
)