import {createSlice} from "@reduxjs/toolkit";
import {login, registration} from "../actions/authActions";


const token = localStorage.getItem('authorization')

const authState = {
    token: token,
    isLogged: !!token,
    loading: false,
    error: "",
    registered: false,
    fieldErrors: []
}

const authSlice = createSlice({
    name: "auth",
    initialState: authState,
    reducers: {
        clearError: state => {
            state.error = ""
            state.fieldErrors = []
        },
        logout: state => {
            state.token = undefined
            localStorage.removeItem("authorization")
            state.isLogged = false
        }
    },
    extraReducers: {
        [registration.pending]: state => {
            state.loading = true
            state.error = ""
            state.fieldErrors = []
        },
        [registration.fulfilled]: state => {
            state.loading = false
        },
        [registration.rejected]: (state, action) => {
            state.loading = false
            if (action.error.name === "Bad Request") {
                state.fieldErrors = JSON.parse(action.error.message)
                state.error = "Форма заполнена с ошибками"
            } else {
                state.error = action.error.message
            }
        },
        [login.pending]: state => {
            state.loading = true
            state.error = ""
            state.fieldErrors = []
        },
        [login.rejected]: (state, action) => {
            state.loading = false
            state.isLogged = false
            state.error = action.error.message
        },
        [login.fulfilled]: (state, action) => {
            state.loading = false
            state.token = action.payload
            state.isLogged = true
        },
    }
})

export const authReducer = authSlice.reducer
export const {clearError, logout} = authSlice.actions