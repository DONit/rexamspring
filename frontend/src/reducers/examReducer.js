import {createSlice} from "@reduxjs/toolkit";
import {createExam, getExams, updateExam} from "../actions/examActions";

const initState = {
    exams: [],
    isLoading: false,
    isLoadingEdit: false,
    error: "",
    fieldErrors: []
}

const examSlice = createSlice({
    name: "exam",
    initialState: initState,
    reducers: {
        clearErrors: state => {
            state.error = ""
            state.fieldErrors = []
        }
    },
    extraReducers: {
        [getExams.pending]: state => {
            state.isLoading = true
        },
        [getExams.rejected]: (state, action) => {
            state.error = action.error.message
        },
        [getExams.fulfilled]: (state, action) => {
            state.exams = action.payload
            state.isLoading = false
        },
        [createExam.pending]: state => {
            state.isLoading = true
        },
        [createExam.rejected]: (state, action) => {
            if (action.error?.name === "Bad Request") {
                state.fieldErrors = JSON.parse(action.error.message)
                state.error = "Форма заполнена с ошибками"
            } else {
                state.error = action.error.message
            }
            state.isLoading = false
        },
        [createExam.fulfilled]: state => {
            state.isLoading = false
        },
        [updateExam.rejected]: (state, action) => {
            if (action.error?.name === "Bad Request") {
                state.fieldErrors = JSON.parse(action.error.message)
                state.error = "Форма заполнена с ошибками"
            } else {
                state.error = action.error.message
            }
            state.isLoadingEdit = false
        },
        [updateExam.pending]: state => {
            state.isLoadingEdit = true
        },
        [updateExam.fulfilled]: state => {
            state.isLoadingEdit = false
        }
    }
})

export const examReducer = examSlice.reducer
export const {clearErrors} = examSlice.actions