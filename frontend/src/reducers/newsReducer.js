import {createSlice} from "@reduxjs/toolkit";
import {createNews, editNews, getNews, removeNews} from "../actions/newsActions";

const newsState = {
    newsList: [],
    isLoading: false,
    error: "",
    listError:"",
    deleted: false,
    isLoadingChange: false,
    successChange: false,
    fieldErrors: [],
}
const newsSlice = createSlice({
    name: "news",
    initialState: newsState,
    extraReducers: {
        [getNews.pending]: state => {
            state.isLoading = true
            state.error = ""
            state.deleted = false
        },
        [getNews.fulfilled]: (state, action) => {
            state.newsList = action.payload
            state.isLoading = false
        },
        [getNews.rejected]: (state, action) => {
            state.listError = action.error.message
            state.isLoading = false
        },
        [createNews.pending]: state => {
            state.isLoading = true
            state.error = ""
        },
        [createNews.rejected]: (state, action) => {
            if (action.error.name === "Bad Request") {
                state.error = "Форма заполнена с ошибками"
                state.fieldErrors = JSON.parse(action.error.message)
            } else {
                state.error = action.error.message
            }
            state.isLoading = false
        },
        [createNews.fulfilled]: state => {
            state.isLoading = false
        },
        [removeNews.fulfilled]: state => {
            state.deleted = true
        },
        [editNews.pending]: state => {
            state.isLoadingChange = true
        },
        [editNews.rejected]: (state, action) => {
            if (action.error.name === "Bad Request") {
                state.error = "Форма заполнена с ошибками"
                state.fieldErrors = JSON.parse(action.error.message)
            } else {
                state.error = action.error.message
            }
            state.isLoadingChange = false
        },
        [editNews.fulfilled]: state => {
            state.isLoadingChange = false
            state.successChange = true
        }
    }, reducers: {
        clearSuccess: state => {
            state.successChange = false
        },
        clearErrors: state => {
            state.error = ""
            state.fieldErrors = ""
            state.listError = ""
        }
    }
})

export const newsReducer = newsSlice.reducer
export const {clearSuccess, clearErrors} = newsSlice.actions