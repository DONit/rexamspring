import {createSlice} from "@reduxjs/toolkit";
import {updateLoginData, updatePrivateData} from "../actions/userActions";

const initState = {
    errorLoginData: "",
    fieldErrorsLoginData: [],
    errorPrivateData: "",
    fieldErrorsPrivateData: [],
    isLoadingLoginData: false,
    isLoadingPrivateData: false,
    successPrivateData: false,
    successLoginData: false,
}

const profileSlice = createSlice({
    name: "profile",
    initialState: initState,
    extraReducers: {
        [updateLoginData.pending]: state => {
            state.errorLoginData = ""
            state.fieldErrorsLoginData = []
            state.isLoadingLoginData = true
        },
        [updateLoginData.rejected]: (state, action) => {
            if (action.error.name === "Bad Request") {
                state.fieldErrorsLoginData = JSON.parse(action.error.message)
                state.errorLoginData = "Форма заполнена с ошибками"
            } else {
                state.errorLoginData = action.error.message
            }
            state.isLoadingLoginData = false
        },
        [updateLoginData.fulfilled]: (state) => {
            state.isLoadingLoginData = false
            state.successLoginData = true
        },
        [updatePrivateData.pending]: (state) => {
            state.isLoadingPrivateData = true
            state.fieldErrorsPrivateData = []
            state.isLoadingPrivateData = true
        },
        [updatePrivateData.fulfilled]: (state) => {
            state.isLoadingPrivateData = false
            state.successPrivateData = true
        },
        [updatePrivateData.rejected]: (state, action) => {
            if (action.error.name === "Bad Request") {
                state.fieldErrorsPrivateData = JSON.parse(action.error.message)
                state.errorPrivateData = "Форма заполнена с ошибками"
            } else {
                state.errorPrivateData = action.error.message
            }
            state.isLoadingPrivateData = false
        }
    },
    reducers:{
        clearLoginDataSuccess:state => {
            state.successLoginData = false
        },
        clearPrivateDataSuccess:state => {
            state.successPrivateData = false
        },
        clearErrorsLoginData:state => {
            state.fieldErrorsLoginData = []
            state.errorLoginData = ""
        },
        clearErrorsPrivateData:state => {
            state.fieldErrorsPrivateData = []
            state.errorPrivateData = ""
        }
    }
})

export const profileReducer = profileSlice.reducer
export const profileActions = profileSlice.actions
export const {clearLoginDataSuccess, clearPrivateDataSuccess, clearErrorsLoginData, clearErrorsPrivateData} = profileActions