import {createSlice} from "@reduxjs/toolkit";
import {questionCreate} from "../actions/questionActions";

const initState = {
    fieldErrors: [],
    error: "",
    isLoading: false
}

const questionSlice = createSlice({
    name: "question",
    initialState: initState,
    reducers: {
        clearErrors: state => {
            state.fieldErrors = []
            state.error = ""
        }
    },
    extraReducers:{
        [questionCreate.pending]: state => {
            state.isLoading = true
        },
        [questionCreate.fulfilled]: state => {
            state.isLoading = false
        },
        [questionCreate.rejected]: (state, action) => {
            if (action.error.name === "Bad Request") {
                state.fieldErrors = JSON.parse(action.error.message)
                state.error = "Форма заполнена с ошибками"
            } else {
                state.error = action.error.message
            }
            state.isLoading = false
        }
    }
})

export const questionReducer = questionSlice.reducer
export const {clearErrors} = questionSlice.actions