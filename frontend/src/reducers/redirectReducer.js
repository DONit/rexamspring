import {createSlice} from "@reduxjs/toolkit";

const redirectSlice = createSlice({
    name: 'redirect',
    initialState: {
        url: null
    },
    reducers: {
        setRedirect: (state, action) => {
            state.url = action.payload
        },
        clearRedirect: state => {
            state.url = null
        }
    }
})

export const redirectReducer = redirectSlice.reducer
export const {setRedirect, clearRedirect} = redirectSlice.actions