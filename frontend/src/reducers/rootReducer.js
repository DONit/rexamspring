import {combineReducers} from "@reduxjs/toolkit";
import {authReducer} from "./authReducer";
import {newsReducer} from "./newsReducer";
import {userReducer} from "./userReducer";
import {redirectReducer} from "./redirectReducer";
import {profileReducer} from "./profileReducer";
import {examReducer} from "./examReducer";
import {questionReducer} from "./quesitonReducer";

export const rootReducer = combineReducers({
    authReducer,
    newsReducer,
    userReducer,
    redirectReducer,
    profileReducer,
    examReducer,
    questionReducer
})