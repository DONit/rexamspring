import {createSlice} from "@reduxjs/toolkit";
import {getScores, getUserData} from "../actions/userActions";

const userState = {
    user: {
        id: "",
        username: "",
        email: "",
        lastName: "",
        firstName: "",
        roles: []
    },
    scores: [],
    isLoading: false,
    isLoadingScore:false,
    errorScore:"",
    error: ""
}

const userSlice = createSlice({
    name: "user",
    initialState: userState,
    extraReducers: {
        [getUserData.pending]: state => {
            state.isLoading = true
            state.error = ""
        },
        [getUserData.fulfilled]: (state, action) => {
            state.user = action.payload
            state.isLoading = false
        },
        [getUserData.rejected]: (state, action) => {
            state.error = action.error.message
            state.isLoading = false
        },
        [getScores.pending]: state => {
            state.isLoadingScore = true
        },
        [getScores.rejected]: (state, action) => {
            state.errorScore = action.error.message
            state.isLoadingScore = false
        },
        [getScores.fulfilled]: (state, action) => {
            state.scores = action.payload
            state.isLoadingScore = false
        }
    }
})

export const userReducer = userSlice.reducer