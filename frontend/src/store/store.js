import {
    configureStore,
    getDefaultMiddleware
} from "@reduxjs/toolkit";
import {rootReducer} from "../reducers/rootReducer";
import {useDispatch} from "react-redux";


const middleware = [
    ...getDefaultMiddleware()
]

const store = configureStore({
    reducer:rootReducer,
    middleware:middleware
})

export default store