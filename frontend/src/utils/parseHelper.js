export const parseErrors = (name, errors) => {
    const mappedErrors = {}
    errors.forEach((item) => {
        mappedErrors[item.field] = item.defaultMessage
    })
    throw {
        name: name,
        message: JSON.stringify(mappedErrors)
    }
}

export const parseDate = (dateString)=>{
    return new Intl.DateTimeFormat('ru', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit'
    }).format(Date.parse(dateString))
}